#include "cpp_parser.hpp"

#include "util.hpp"

#include <clang/AST/Comment.h>
#include <clang/AST/PrettyPrinter.h>
#include <clang/AST/RecursiveASTVisitor.h>
#include <clang/Frontend/CompilerInstance.h>
#include <clang/Frontend/FrontendActions.h>
#include <clang/Tooling/CommonOptionsParser.h>
#include <clang/Tooling/Tooling.h>
#include <llvm/Support/CommandLine.h>

#include <filesystem>
#include <iostream>

using namespace std::string_view_literals;

namespace ucpp {

	static bool is_excluded_impl(const clang::NamedDecl& decl, bool keep_out_of_line = false)
	{
		auto& ctx = decl.getASTContext();
		if(ctx.getSourceManager().getMainFileID() != ctx.getSourceManager().getFileID(decl.getLocation()))
			return true;

		if(decl.getAccess() == clang::AS_private)
			return true;

		if(!keep_out_of_line && decl.isOutOfLine())
			return true;

		if(!decl.isDefinedOutsideFunctionOrMethod())
			return true;

		if(auto rd = llvm::dyn_cast<clang::CXXRecordDecl>(&decl)) {
			if(auto def = rd->getDefinition(); def && def != rd)
				return true;
		}

		if(contains(comment(decl), "\\skip"))
			return true;

		if(decl.getQualifiedNameAsString().starts_with("std::"))
			return true;

		return false;
	}

	static bool is_primary_declaration(const clang::NamedDecl& decl)
	{
		auto& ctx = decl.getASTContext();

		// declaration with a doc-comment
		if(!comment(decl).empty())
			return true;

		// check all other declarations
		const clang::Decl* last_decl = nullptr;
		for(const clang::Decl* d = decl.getMostRecentDecl(); d; d = d->getPreviousDecl()) {
			auto nd = llvm::dyn_cast<const clang::NamedDecl>(d);
			if(!nd || is_excluded_impl(*nd, true))
				continue;

			if(!last_decl)
				last_decl = d;

			// always prefer the documented declaration
			if(!comment(*d, &ctx).empty())
				return d == &decl;
		}

		return last_decl == &decl;
	}

	bool Parser_db::is_excluded(const clang::NamedDecl& decl, bool keep_out_of_line) const
	{
		if(is_excluded_impl(decl, keep_out_of_line) || !is_primary_declaration(decl))
			return true;

		if(finalized_ && !decl.isOutOfLine()) {
			auto key = "::" + name(decl);
			std::reverse(key.begin(), key.end());

			if(auto iter = entities_.find(Entity_type::type_t); iter != entities_.end()) {
				if(auto existing = iter->second.find(key); existing != iter->second.end()) {
					return std::any_of(existing->begin(), existing->end(), [&](auto& e) {
						auto other_decl = std::get_if<clang::NamedDecl*>(&e);
						return other_decl && *other_decl != &decl;
					});
				}
			}
		}

		return false;
	}

	static std::optional<std::string> example_name(clang::FunctionDecl& decl)
	{
		auto& ctx = decl.getASTContext();
		auto& sm  = ctx.getSourceManager();

		if(!decl.getBody() || sm.getMainFileID() != sm.getFileID(decl.getBodyRBrace()))
			return std::nullopt;

		if(auto comment_str = comment(decl, &ctx); !comment_str.empty()) {
			if(auto begin = comment_str.find("\\def_example{"); begin != std::string::npos) {
				if(auto end = comment_str.find("}", begin); end != std::string::npos) {
					return comment_str.substr(begin + 13, end - begin - 13);
				}
			}
		}


		auto range    = clang::SourceRange(decl.getSourceRange().getBegin(),
                                        decl.getBody()->getSourceRange().getBegin());
		auto decl_str = clang::Lexer::getSourceText(
		        clang::CharSourceRange::getTokenRange(range), sm, ctx.getLangOpts());

		if(decl_str.startswith("TEST_CASE(")) {
			auto begin = decl_str.find_first_of('"');
			auto end   = decl_str.find_last_of('"');
			if(begin != std::string::npos && end != std::string::npos && begin < end)
				return decl_str.substr(begin + 1, end - begin - 1).str();
		}

		return std::nullopt;
	}

	std::string name(const clang::NamedDecl& decl)
	{
		if(auto r = llvm::dyn_cast<clang::CXXRecordDecl>(&decl)) {
			if(auto d = r->getTypedefNameForAnonDecl()) {
				return d->getQualifiedNameAsString();
			}
		}

		return decl.getQualifiedNameAsString();
	}
	std::string id(const clang::NamedDecl& decl)
	{
		// special case for hidden friend functions, which we want to inject into their lexical scope, so we can later difference between overloads from different classes
		if(auto f = llvm::dyn_cast<clang::FunctionDecl>(&decl);
		   f && f == f->getDefinition() && f->getLexicalParent() != f->getParent()) {
			if(auto parent = llvm::dyn_cast<clang::RecordDecl>(f->getLexicalParent())) {
				const auto functionName = decl.getQualifiedNameAsString();
				const auto last_sep     = functionName.rfind(':');
				auto       r            = id(*parent)
				         + "::" + functionName.substr(last_sep != std::string::npos ? last_sep + 1 : 0);
				return r;
			}
		}
		return "::" + decl.getQualifiedNameAsString();
	}

	static clang::RawComment* getRawCommentForDeclNoCache(const clang::ASTContext* ctx, const clang::Decl* D)
	{
		auto c = ctx->getRawCommentForDeclNoCache(D);
		if(c)
			return c;

		// libtooling ignores trailing comments for some types of entities, so we need to reproduce
		//   some of their code here, to suppress this
		auto&                       srcManager = const_cast<clang::SourceManager&>(ctx->getSourceManager());
		const clang::SourceLocation DeclLoc    = D->getLocation();

		// If the declaration doesn't map directly to a location in a file, we
		// can't find the comment.
		if(DeclLoc.isInvalid() || !DeclLoc.isFileID())
			return nullptr;

		if(ctx->ExternalSource && !ctx->CommentsLoaded) {
			ctx->ExternalSource->ReadComments();
			ctx->CommentsLoaded = true;
		}

		if(ctx->Comments.empty())
			return nullptr;

		auto File = srcManager.getDecomposedLoc(DeclLoc).first;
		if(!File.isValid()) {
			return nullptr;
		}
		const auto CommentsInThisFile = ctx->Comments.getCommentsInFile(File);
		if(!CommentsInThisFile || CommentsInThisFile->empty())
			return nullptr;

		// Decompose the location for the declaration and find the beginning of the
		// file buffer.
		auto DeclLocDecomp = srcManager.getDecomposedLoc(DeclLoc);

		// Slow path.
		auto OffsetCommentBehindDecl = CommentsInThisFile->lower_bound(DeclLocDecomp.second);

		// First check whether we have a trailing comment.
		if(OffsetCommentBehindDecl != CommentsInThisFile->end()) {
			auto* CommentBehindDecl = OffsetCommentBehindDecl->second;
			if(CommentBehindDecl->isDocumentation() && CommentBehindDecl->isTrailingComment()) {
				// Check that Doxygen trailing comment comes after the declaration, starts
				// on the same line and in the same file as the declaration.
				if(srcManager.getLineNumber(DeclLocDecomp.first, DeclLocDecomp.second)
				   == ctx->Comments.getCommentBeginLine(
				           CommentBehindDecl, DeclLocDecomp.first, OffsetCommentBehindDecl->first)) {
					return CommentBehindDecl;
				}
			}
		}

		return nullptr;
	}

	static std::string format(const clang::ASTContext&         ctx,
	                          const clang::RawComment&         raw_comment,
	                          std::vector<clang::PresumedLoc>* out_line_locs)
	{
		if(!out_line_locs) {
			return raw_comment.getFormattedText(ctx.getSourceManager(), ctx.getDiagnostics());
		} else {
			auto lines = raw_comment.getFormattedLines(ctx.getSourceManager(), ctx.getDiagnostics());
			auto str   = std::string();
			for(auto& line : lines) {
				str += line.Text;
				str += '\n';
				out_line_locs->push_back(line.Begin);
			}
			return str;
		}
	}
	static void fix_indent(std::string& comment, std::vector<clang::PresumedLoc>* out_line_locs)
	{
		auto removed_indent = remove_redundant_indentation(comment);
		if(out_line_locs && removed_indent > 0) {
			for(auto& loc : *out_line_locs) {
				loc = clang::PresumedLoc(loc.getFilename(),
				                         loc.getFileID(),
				                         loc.getLine(),
				                         loc.getColumn() + removed_indent,
				                         loc.getIncludeLoc());
			}
		}
	}

	std::string comment(const Entity&                    e,
	                    const clang::ASTContext*         ctx_param,
	                    std::vector<clang::PresumedLoc>* out_line_locs)
	{
		auto str = std::visit(overloaded{[](const File*) { return std::string(); },
		                                 [&](const Macro* m) {
			                                 auto& ctx = ctx_param ? *ctx_param : *m->ctx;
			                                 if(m->comment) {
				                                 return format(ctx, *m->comment, out_line_locs);
			                                 } else {
				                                 return std::string();
			                                 }
		                                 },
		                                 [&](const clang::Decl* d) {
			                                 auto& ctx = ctx_param ? *ctx_param : d->getASTContext();
			                                 if(auto raw_comment = getRawCommentForDeclNoCache(&ctx, d)) {
				                                 return format(ctx, *raw_comment, out_line_locs);
			                                 } else {
				                                 return std::string();
			                                 }
		                                 }},
		                      e);
		fix_indent(str, out_line_locs);

		return str;
	}
	std::string comment(const clang::Decl&               d,
	                    const clang::ASTContext*         ctx_param,
	                    std::vector<clang::PresumedLoc>* out_line_locs)
	{
		auto& ctx = ctx_param ? *ctx_param : d.getASTContext();
		if(auto raw_comment = getRawCommentForDeclNoCache(&ctx, &d)) {
			auto str = format(ctx, *raw_comment, out_line_locs);

			fix_indent(str, out_line_locs);

			return str;

		} else {
			return {};
		}
	}
	bool comment_empty(const clang::Decl& d)
	{
		auto& ctx = d.getASTContext();
		if(auto raw_comment = getRawCommentForDeclNoCache(&ctx, &d)) {
			auto str = raw_comment->getFormattedText(ctx.getSourceManager(), ctx.getDiagnostics());
			return str.find_first_not_of(" \t\n\r");

		} else {
			return true;
		}
	}

	Parser_db::Parser_db()  = default;
	Parser_db::~Parser_db() = default;

	Entity_list Parser_db::lookup(Entity_type type, std::string_view reference_string) const
	{
		if(auto iter = entities_.find(type); iter != entities_.end()) {
			auto key = std::string();
			key.reserve(reference_string.length() + 2);
			std::reverse_copy(
			        reference_string.begin(), reference_string.end(), std::back_insert_iterator(key));

			const auto sep = type == Entity_type::file_t ? "/"sv : "::"sv;

			const auto absolute = key.ends_with(sep);

			// always end name with separator to avoid matching actual prefixes
			if(!absolute) {
				key.append(sep);
			}

			// return all entities that match the given reference
			auto collect = [&](auto&& range) {
				auto entities = Entity_list{};
				for(auto iter = range.first; iter != range.second; ++iter) {
					entities.insert(entities.end(), iter->begin(), iter->end());
				}

				return entities;
			};

			if(absolute)
				return collect(iter->second.equal_range(key));
			else
				return collect(iter->second.equal_prefix_range(key));

		} else {
			return {};
		}
	}

	void Parser_db::register_entity(Entity_type type, Entity e)
	{
		auto key = std::visit(overloaded{[](const clang::NamedDecl* decl) { return id(*decl); },
		                                 [](const File* file) { return file->path; },
		                                 [](const Macro* macro) { return "::" + macro->identifier; }},
		                      e);

		std::reverse(key.begin(), key.end());
		entities_[type][key].push_back(e);

		if(auto decl = std::get_if<clang::NamedDecl*>(&e)) {
			if(auto td = llvm::dyn_cast<clang::TypedefNameDecl>(*decl)) {
				auto type = td->getUnderlyingType().getAsString(default_printing_policy());
				name_to_aliases_[type].emplace_back(name(**decl));
			}
		}
	}

	std::filesystem::path Parser_db::to_relative_path(const std::filesystem::path& path) const
	{
		if(path.is_relative())
			return path;

		auto shortest_path   = path;
		auto shortest_length = std::distance(path.begin(), path.end());
		for(auto& base : base_directories_) {
			auto p      = proximate(path, base);
			auto length = std::distance(p.begin(), p.end());
			if(length < shortest_length) {
				shortest_path   = std::move(p);
				shortest_length = length;
			}
		}

		return shortest_path;
	}

	void Parser_db::register_example(const std::string& name, clang::FunctionDecl* decl)
	{
		if(auto&& [iter, inserted] = named_examples_.emplace(name, decl); !inserted) {
			auto& sm    = decl->getASTContext().getSourceManager();
			auto  loc_1 = iter->second->getBeginLoc();
			auto  loc_2 = decl->getBeginLoc();

			std::cerr << "Multiple named examples with the same name \"" << name << "\":\n";
			std::cerr << "  1. " << sm.getFilename(loc_1).str() << ':' << sm.getSpellingLineNumber(loc_1)
			          << '\n';
			std::cerr << "  2. " << sm.getFilename(loc_2).str() << ':' << sm.getSpellingLineNumber(loc_2)
			          << '\n';
		}
	}
	clang::FunctionDecl* Parser_db::get_example(const std::string& name) const
	{
		if(auto iter = named_examples_.find(name); iter != named_examples_.end()) {
			return iter->second;
		} else {
			return nullptr;
		}
	}

	namespace {
		class Visitor : public clang::RecursiveASTVisitor<Visitor> {
		  public:
			Parser_db& db;
			int        skip_namespace = 0;

			Visitor(Parser_db& db) : db(db) {}

			bool TraverseNamespaceDecl(clang::NamespaceDecl* decl)
			{
				if(decl->isAnonymousNamespace() || decl->getName() == "detail"
				   || decl->getName() == "details") {
					skip_namespace++;
					auto r = RecursiveASTVisitor::TraverseNamespaceDecl(decl);
					skip_namespace--;
					return r;

				} else {
					return RecursiveASTVisitor::TraverseNamespaceDecl(decl);
				}
			}

			bool TraverseVarTemplateSpecializationDecl(clang::VarTemplateSpecializationDecl*) { return true; }
			bool TraverseVarTemplatePartialSpecializationDecl(clang::VarTemplatePartialSpecializationDecl*)
			{
				return true;
			}

			static const clang::CXXRecordDecl* find_spec_decl(clang::ClassTemplateSpecializationDecl* decl)
			{
				auto t = decl->getTemplateArgs().get(0).getAsType();

				auto base = t->getAsCXXRecordDecl();

				if(!base) {
					if(auto spec_t = t->getAs<clang::TemplateSpecializationType>()) {
						if(auto td = spec_t->getTemplateName().getAsTemplateDecl()) {
							if(auto ctd = llvm::dyn_cast<clang::ClassTemplateDecl>(td)) {
								base = ctd->getTemplatedDecl();
							}
						}
					}
				}

				return base;
			}

			bool TraverseClassTemplateSpecializationDecl(clang::ClassTemplateSpecializationDecl* decl)
			{
				if(decl->getTemplateArgs().size() == 1) {
					auto decl_name = name(*decl);
					if(decl_name == "std::hash") {
						if(auto t = find_spec_decl(decl)) {
							db.set_hashable(name(*t));
						}
					} else if(decl_name == "std::tuple_size") {
						if(auto t = find_spec_decl(decl)) {
							db.set_tuple_like(name(*t));
						}
					}
				}
				return true;
			}
			bool TraverseClassTemplatePartialSpecializationDecl(
			        clang::ClassTemplatePartialSpecializationDecl* decl)
			{
				return TraverseClassTemplateSpecializationDecl(decl);
			}


#define UCPP_AST_VISITOR(IN_T, OUT_T)                       \
	bool Visit##IN_T(clang::IN_T* decl)                     \
	{                                                       \
		if(skip_namespace == 0 && !db.is_excluded(*decl)) { \
			db.register_entity(OUT_T, decl);                \
		}                                                   \
		return true;                                        \
	}

			UCPP_AST_VISITOR(RecordDecl, Entity_type::type_t)
			UCPP_AST_VISITOR(ConceptDecl, Entity_type::concept_t)
			UCPP_AST_VISITOR(TypedefDecl, Entity_type::type_t)
			UCPP_AST_VISITOR(TypeAliasDecl, Entity_type::type_t)
			UCPP_AST_VISITOR(EnumDecl, Entity_type::type_t)
			UCPP_AST_VISITOR(FieldDecl, Entity_type::variable_t)

			bool VisitFunctionDecl(clang::FunctionDecl* decl)
			{
				if(auto name = example_name(*decl)) {
					db.register_example(*name, decl);
				} else if(skip_namespace == 0 && !db.is_excluded(*decl, true)) {
					db.register_entity(Entity_type::function_t, decl);
				}
				return true;
			}

			bool VisitVarDecl(clang::VarDecl* decl)
			{
				if(skip_namespace == 0 && !db.is_excluded(*decl) && !decl->isLocalVarDeclOrParm()) {
					db.register_entity(Entity_type::variable_t, decl);
				}
				return true;
			}

#undef UCPP_AST_VISITOR
		};

	} // namespace

	Parser_db Parser_db::parse(compile_config& config, const std::vector<std::string>& paths)
	{
		using namespace std::filesystem;

		auto db = Parser_db{};

		for(auto& filename : paths) {
			const auto p = path(filename);
			if(is_directory(p)) {
				db.base_directories_.emplace_back(p);
				config.add_include_dir(absolute(p).generic_string());
			}
		}

		auto files = std::vector<std::string>();
		for(auto& filename : paths) {
			const auto p = path(filename);
			if(is_directory(p)) {
				for(auto& sub_p : recursive_directory_iterator(p)) {
					if(sub_p.is_regular_file()
					   && (sub_p.path().extension() == ".h" || sub_p.path().extension() == ".hpp"
					       || sub_p.path().extension() == ".hxx" || sub_p.path().extension() == ".h++")) {
						files.push_back(sub_p.path().generic_string());
					}
				}

			} else if(is_regular_file(p)) {
				files.push_back(p.generic_string());
			}
		}

		for(auto& f : files) {
			std::cout << "Parsing C++ file " << f << ":1 \n";
		}

		using namespace clang::tooling;

		llvm::cl::OptionCategory MyToolCategory("my-tool options");

		auto argv = std::vector<const char*>{"dummy",
		                                     "dummy",
		                                     "--",
		                                     "-Wno-unknown-attributes",
		                                     "-Wdocumentation",
		                                     "-Wno-pragma-once-outside-header"};
		config.get_arguments(argv);

		int  argc = argv.size();
		auto options_parser =
		        CommonOptionsParser::create(argc, argv.data(), MyToolCategory, llvm::cl::OneOrMore);

		auto tool = ClangTool{options_parser->getCompilations(), files};
		auto asts = std::vector<std::unique_ptr<clang::ASTUnit>>();

		tool.buildASTs(asts);

		db.files_.reserve(asts.size());
		for(auto& ast : asts) {
			auto  root = ast->getASTContext().getTranslationUnitDecl();
			auto& file = db.files_.emplace_back(File{ast->getMainFileName().str(), std::move(ast), root, {}});
			db.register_entity(Entity_type::file_t, &file);
		}

		db.do_parse();

		return db;
	}


	clang::PrintingPolicy default_printing_policy()
	{
		auto lang        = clang::LangOptions{};
		lang.CPlusPlus   = true;
		lang.CPlusPlus11 = true;

		auto pp                                = clang::PrintingPolicy{lang};
		pp.TerseOutput                         = true;
		pp.FullyQualifiedName                  = false;
		pp.IncludeNewlines                     = false;
		pp.Bool                                = true;
		pp.Nullptr                             = true;
		pp.SuppressDefaultTemplateArgs         = true;
		pp.SuppressInlineNamespace             = true;
		pp.SuppressUnwrittenScope              = true;
		pp.PolishForDeclaration                = false;
		pp.PrintInjectedClassNameWithArguments = false;
		return pp;
	}

	void Parser_db::do_parse()
	{
		assert(!finalized_ && "Parser_db::do_parse() has already been called for this instance");

		for(auto& file : files_) {
			auto ast = file.unit.get();
			ast->getASTContext().setPrintingPolicy(default_printing_policy());

			const auto fileId = ast->getSourceManager().getMainFileID();
			if(const auto* comments = ast->getASTContext().Comments.getCommentsInFile(fileId)) {
				for(auto&& [id, info] : ast->getPreprocessor().macros(false)) {
					auto& md = *info.getLatest();
					if(md.isDefined()) {
						if(ast->getSourceManager().getFileID(md.getDefinition().getLocation()) == fileId
						   && !md.getMacroInfo()->isBuiltinMacro()
						   && !md.getMacroInfo()->isUsedForHeaderGuard()) {

							// try to find matching comment and ignore macros without comments
							const auto macro_line =
							        ast->getSourceManager().getSpellingLineNumber(md.getLocation());
							for(auto&& [_, c] : *comments) {
								if(c->isAttached())
									continue;

								const auto line = ast->getSourceManager().getSpellingLineNumber(
								        c->getSourceRange().getEnd());

								if(line < macro_line && (macro_line - line) <= 2) {
									auto& ptr = file.macros.emplace_back(
									        std::make_unique<Macro>(Macro{id->getName().str(),
									                                      md.getMacroInfo(),
									                                      c,
									                                      &ast->getASTContext()}));

									register_entity(Entity_type::macro_t, ptr.get());
									break;
								}
							}
						}
					}
				}
			}

			Visitor{*this}.TraverseDecl(file.root);
		}

		// remove forward-declarations, if there is at least one definition in any of the files
		for(auto& type_declarations : entities_[Entity_type::type_t]) {
			if(type_declarations.size() <= 1)
				continue;

			auto new_type_declarations = type_declarations;
			std::erase_if(new_type_declarations,
			              [&](auto& decl_v) { return is_excluded(*std::get<clang::NamedDecl*>(decl_v)); });
			if(new_type_declarations.empty()) {
				// all declarations are equally bad => keep the first one
				type_declarations.resize(1);
				continue;
			}

			// if any of the declarations has a doc-comment => remove all undocumented once
			if(std::any_of(new_type_declarations.begin(), new_type_declarations.end(), [&](auto& decl_v) {
				   return !comment_empty(*std::get<clang::NamedDecl*>(decl_v));
			   })) {
				std::erase_if(new_type_declarations, [&](auto& decl_v) {
					return comment_empty(*std::get<clang::NamedDecl*>(decl_v));
				});
			}
			if(new_type_declarations.size() == 1) {
				type_declarations = new_type_declarations;
				continue;
			}


			auto best_decl = static_cast<Entity*>(nullptr);
			for(auto& decl_v : new_type_declarations) {
				auto decl = std::get<clang::NamedDecl*>(decl_v);

				if(auto rd = llvm::dyn_cast<clang::CXXRecordDecl>(decl); rd && rd->getDefinition()) {
					best_decl = &decl_v;
				}
			}

			if(!best_decl) {
				best_decl = &new_type_declarations.front();
			}
			type_declarations.clear();
			type_declarations.emplace_back(*best_decl);
		}

		finalized_ = true;
	}

} // namespace ucpp
