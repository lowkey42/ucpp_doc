#include "rst_writer.hpp"

#include "util.hpp"

#include <clang/AST/Comment.h>
#include <clang/AST/PrettyPrinter.h>
#include <clang/AST/RecursiveASTVisitor.h>
#include <clang/ASTMatchers/ASTMatchFinder.h>
#include <clang/ASTMatchers/ASTMatchers.h>
#include <clang/Basic/SourceManager.h>
#include <clang/Lex/Lexer.h>
#include <clang/Lex/MacroInfo.h>

#include <bitset>
#include <map>
#include <unordered_set>

using namespace std::string_view_literals;

static std::ostream& operator<<(std::ostream& os, const clang::StringRef& str)
{
	return os << std::string_view(str.data(), str.size());
}

namespace ucpp {
	namespace {
		constexpr auto push = indent::push(2);
		constexpr auto pop  = indent::pop(2);

		std::string_view remove_common_scope(std::string_view identifier, std::string_view base_scope)
		{
			auto [p, _] =
			        std::mismatch(identifier.begin(), identifier.end(), base_scope.begin(), base_scope.end());
			return std::find(std::reverse_iterator(p), std::reverse_iterator(identifier.begin()), ':').base();
		}

		std::string_view next_word(std::string_view str, std::size_t& index_inout)
		{
			if(index_inout >= str.size())
				return {};

			auto end    = std::min(str.size(), str.find_first_of(" \t\v\n\r\f{\\", index_inout + 1));
			auto r      = str.substr(index_inout + 1, end - (index_inout + 1));
			index_inout = end - 1;
			if(end < str.size() && str[end] == ' ')
				index_inout = end; // skip space after command
			return r;
		}
		std::optional<std::string_view> next_arg(std::string_view str, std::size_t& index_inout)
		{
			if(index_inout >= str.size())
				return {};

			auto begin = str.find_first_of("\n{", index_inout);
			if(begin == std::string_view::npos || str[begin] != '{')
				return std::nullopt;

			auto end = str.find_first_of("}\n", begin + 1);
			if(end == std::string_view::npos || str[end] != '}')
				return std::nullopt;

			auto r      = str.substr(begin + 1, end - (begin + 1));
			index_inout = end + 1;
			return r;
		}
		void read_until_marker(std::ostream&    out,
		                       std::string_view str,
		                       std::string_view marker,
		                       std::size_t&     index_inout)
		{
			if(index_inout >= str.size())
				return;

			for(; index_inout < str.size(); ++index_inout) {
				if(str[index_inout] == '\\' && str.substr(index_inout).starts_with(marker)) {
					index_inout += marker.size();
					break;
				}

				out << str[index_inout];
			}
		}

		template <typename F>
		void foreach_conjunction_term(clang::Expr* clause, F&& callback)
		{
			if(auto op = dyn_cast<clang::BinaryOperator>(clause->IgnoreParenImpCasts())) {
				if(op->getOpcode() == clang::BO_LAnd) {
					foreach_conjunction_term(op->getLHS(), callback);
					foreach_conjunction_term(op->getRHS(), callback);
				}

				return;
			} else {
				callback(clause);
			}
		}

		class BoolMatchCallback final : public clang::ast_matchers::MatchFinder::MatchCallback {
		  public:
			bool check() const { return found_; }
			void run(const clang::ast_matchers::MatchFinder::MatchResult&) override { found_ = true; }

		  private:
			bool found_ = false;
		};

		template <typename Node, typename Matcher>
		bool matches(clang::ASTContext& ctx, Node& node, Matcher& matcher)
		{
			auto found  = BoolMatchCallback{};
			auto finder = clang::ast_matchers::MatchFinder();
			finder.addMatcher(matcher, &found);
			finder.template match(node, ctx);
			return found.check();
		}

		class Visitor : public clang::RecursiveASTVisitor<Visitor>, private clang::PrintingCallbacks {
		  public:
			struct Access {
				clang::AccessSpecifier specifier;
				bool                   written = false;
			};
			struct Scope {
				clang::DeclContext* context;
				std::string         qualified_name;
			};

			std::ostream&                   out;
			const Parser_db&                db;
			std::unordered_set<std::string> written_functions;
			std::vector<Access>             access;
			std::optional<std::string>      last_written_scope;
			std::vector<Scope>              namespace_scopes;
			std::vector<Scope>              scopes;
			std::string                     current_scope;
			clang::PrintingPolicy           printing_policy = default_printing_policy();

			Visitor(std::ostream& out, const Parser_db& db) : out(out), db(db)
			{
				printing_policy.Callbacks = this;
			}

			static void write_location(const clang::ASTContext& ctx, std::ostream& out, const Entity& e)
			{
				std::visit(overloaded{[](const File*) {},
				                      [&](const Macro* m) {
					                      auto&& loc = m->info->getDefinitionLoc();
					                      auto&  sm  = ctx.getSourceManager();
					                      out << "[ucpp_src_location: " << sm.getFilename(loc).str() << "; "
					                          << sm.getSpellingLineNumber(loc) << "]\n\n";
				                      },
				                      [&](const clang::Decl* d) {
					                      auto&& loc = d->getLocation();
					                      auto&  sm  = ctx.getSourceManager();
					                      out << "[ucpp_src_location: " << sm.getFilename(loc).str() << "; "
					                          << sm.getSpellingLineNumber(loc) << "]\n\n";
				                      }},
				           e);
			}

			static std::ostream& log_error(const clang::ASTContext&   ctx,
			                               const Entity&              e,
			                               std::optional<std::size_t> comment_offset = std::nullopt)
			{
				std::cerr << "Error in entity at ";

				if(comment_offset) {
					// prints the line number for a comment block, approximating the column and line numbers based on comment_offset
					auto locs = std::vector<clang::PresumedLoc>();
					auto c    = comment(e, &ctx, &locs);

					auto line_number     = std::count(c.begin(), c.begin() + *comment_offset, '\n');
					auto last_line_break = c.substr(0, *comment_offset).rfind('\n');

					if(std::size_t(line_number) < locs.size()) {
						const auto& loc = locs[line_number];
						const auto  column =
						        loc.getColumn()
						        + (*comment_offset
						           - (last_line_break != std::string_view::npos ? last_line_break : 0));

						std::cerr << loc.getFilename() << ':' << loc.getLine() << ':' << column;

					} else if(!locs.empty()) {
						std::cerr << locs.back().getFilename() << ':' << (locs.back().getLine() + 1);

					} else {
						auto& sm = ctx.getSourceManager();
						std::visit(overloaded{[](const File* f) { std::cerr << f->path << ":0"; },
						                      [&](const Macro* m) {
							                      std::cerr
							                              << sm.getFilename(m->info->getDefinitionLoc()).str()
							                              << ":0";
						                      },
						                      [&](const clang::Decl* d) {
							                      std::cerr << sm.getFilename(d->getLocation()).str() << ":0";
						                      }},
						           e);
					}

				} else {
					auto print_location = [&](const clang::SourceLocation& loc) {
						auto& sm = ctx.getSourceManager();
						std::cerr << sm.getFilename(loc).str() << ':' << sm.getSpellingLineNumber(loc) << ':'
						          << sm.getSpellingColumnNumber(loc);
					};

					std::visit(
					        overloaded{[](const File* f) { std::cerr << f->path << ":1"; },
					                   [&](const Macro* m) { print_location(m->info->getDefinitionLoc()); },
					                   [&](const clang::Decl* d) { print_location(d->getLocation()); }},
					        e);
				}

				std::cerr << ": ";

				return std::cerr;
			}

			/// OnSpecialSection(auto... section_name_parts) -> std::ostream*
			template <typename OnSpecialSection>
			void process_comment(const clang::ASTContext& ctx,
			                     std::ostream&            main_out,
			                     const Entity&            e,
			                     std::string_view         comment,
			                     std::size_t              comment_offset,
			                     OnSpecialSection&&       on_special_section)
			{
				auto out = &main_out;

				auto in_section = false;

				auto current_section = std::optional<std::string_view>();

				auto end_section = [&] {
					if(out != &main_out) {
						out = &main_out; // was special section

					} else if(in_section) {
						in_section = false;
						current_section.reset();
						*out << pop << '\n';
					}
				};
				auto start_section = [&](std::string_view key, auto&&... vs) {
					if(key == current_section) {
						*out << '\n';
						return;
					}

					end_section();
					*out << '\n';
					*out << key;
					(void) (*out << ... << vs);
					in_section      = true;
					current_section = key;
					*out << push;
				};
				auto start_special_section = [&](auto&&... vs) {
					end_section();

					auto new_out = on_special_section(vs...);
					if(new_out != nullptr) {
						out = new_out;
					} else {
						start_section(vs...);
					}
				};

				for(std::size_t i = 0; i < comment.size(); ++i) {
					if(in_section && comment[i] == '\n' && i + 1 < comment.size() && comment[i + 1] == '\n')
						end_section();

					if(comment[i] != '\\' && comment[i] != '@') {
						*out << comment[i];

					} else if(comment[i] == '\\' && i + 1 < comment.size() && comment[i + 1] == '\\') {
						*out << "\\";
						i++;

					} else {
						auto       index   = i;
						const auto command = next_word(comment, index);

						if(command == "n" || command == "par") {
							// command should start a new paragraph => insert an empty line
							end_section();
							*out << "\n\n";

						} else if(command == "tparam") {
							start_special_section(":tparam ", next_word(comment, index), ": ");

						} else if(command == "param") {
							start_special_section(":param ", next_word(comment, index), ": ");

						} else if(command == "rparam") {
							start_special_section(":rparam ", next_word(comment, index), ": ");

						} else if(command == "return" || command == "returns") {
							start_special_section(":return: ");

						} else if(command == "throw" || command == "throws") {
							start_special_section(":exception ", next_word(comment, index), ": ");

						} else if(command == "pre") {
							start_special_section(":precondition: ");
						} else if(command == "post") {
							start_special_section(":postcondition: ");
						} else if(command == "invariant") {
							start_special_section(":invariant: ");

						} else if(command == "brief") {
							// just skip the command and directly copy its content
							end_section();

						} else if(command == "overload_specific") {
							current_section.reset(); // terminate sections early

							log_error(ctx, e, i + comment_offset)
							        << "Found \\overload_specific in comment that is not part of a "
							           "function overload-set.\n";

						} else if(command == "include_example") {
							end_section();
							if(auto name = next_arg(comment, index)) {
								if(auto example = db.get_example(std::string(*name))) {
									write_example(*out, *example);

								} else {
									log_error(ctx, e, i + comment_offset)
									        << "Reference to unknown example '" << *name << "'\n";
									*out << ".. ERROR::\n";
									*out << "  Reference to unknown example '" << *name << "'\n";
								}
							}

						} else if(command == "author" || command == "authors") {
							start_section(".. codeauthor:: ");

						} else if(command == "todo") {
							start_section(".. admonition:: TODO\n\n");

						} else if(command == "see") {
							start_section(".. seealso::\n\n");

						} else if(command == "warning") {
							start_section(".. warning::\n\n");

						} else if(command == "attention") {
							start_section(".. attention::\n\n");

						} else if(command == "note") {
							start_section(".. note::\n\n");

						} else if(command == "tip") {
							start_section(".. tip::\n\n");

						} else if(command == "danger") {
							start_section(".. danger::\n\n");

						} else if(command == "verbatim") {
							start_section("::\n");
							read_until_marker(*out, comment, "\\endverbatim", index);
							end_section();

						} else if(command == "code") {
							const auto lang = next_arg(comment, index).value_or("cpp");
							start_section(".. code-block:: ", lang, "\n");
							*out << ":linenos:\n\n";
							read_until_marker(*out, comment, "\\endcode", index);
							end_section();

						} else if(command == "skip") {
							// ignore

						} else {
							log_error(ctx, e, i + comment_offset)
							        << "Unknown doxygen command '" << command << "' is being ignored.\n";
							*out << comment[i];
						}

						// skipped the command we just parsed
						i = index;

						if(i >= comment.size())
							break; // reached the end of the input
					}
				}

				end_section();
			}

			static bool has_equality_compare(clang::CXXRecordDecl& decl)
			{
				using namespace clang;
				using namespace clang::ast_matchers;

				// clang-format off
				auto matcher = cxxRecordDecl(
					anyOf(
						// member (by-reference and by-value)
						hasMethod(
						cxxMethodDecl(
							hasName("operator=="),
							unless(isDeleted()),
							isPublic(),
							parameterCountIs(1), hasParameter(0,
								parmVarDecl(hasType(references(cxxRecordDecl(equalsNode(&decl))))))
						)),
						hasMethod(
						cxxMethodDecl(
							hasName("operator=="),
							unless(isDeleted()),
							isPublic(),
							parameterCountIs(1), hasParameter(0,
								parmVarDecl(hasType(cxxRecordDecl(equalsNode(&decl)))))
						)),
						
						// hidden friend (by-reference and by-value)
						has(friendDecl(has(functionDecl(
							hasName("operator=="),
							unless(isDeleted()),
							isPublic(),
							parameterCountIs(2),
							hasParameter(0, parmVarDecl(hasType(references(cxxRecordDecl(equalsNode(&decl)))))),
							hasParameter(1, parmVarDecl(hasType(references(cxxRecordDecl(equalsNode(&decl))))))
						)))),
						has(friendDecl(has(functionDecl(
							hasName("operator=="),
							unless(isDeleted()),
							isPublic(),
							parameterCountIs(2),
							hasParameter(0, parmVarDecl(hasType(cxxRecordDecl(equalsNode(&decl))))),
							hasParameter(1, parmVarDecl(hasType(cxxRecordDecl(equalsNode(&decl)))))
						)))),
						
						// free function (by-reference and by-value)
						hasAncestor(namespaceDecl(has( functionDecl(
							hasName("operator=="),
							unless(isDeleted()),
							parameterCountIs(2),
							hasParameter(0, parmVarDecl(hasType(references(cxxRecordDecl(equalsNode(&decl)))))),
							hasParameter(1, parmVarDecl(hasType(references(cxxRecordDecl(equalsNode(&decl))))))
						)))),
						hasAncestor(namespaceDecl(has( functionDecl(
							hasName("operator=="),
							unless(isDeleted()),
							parameterCountIs(2),
							hasParameter(0, parmVarDecl(hasType(cxxRecordDecl(equalsNode(&decl))))),
							hasParameter(1, parmVarDecl(hasType(cxxRecordDecl(equalsNode(&decl)))))
						))))
						
					)
				);
				// clang-format on

				return matches(decl.getASTContext(), decl, matcher);
			}

			static std::optional<clang::QualType> get_order_type(clang::CXXRecordDecl& decl)
			{
				using namespace clang;
				using namespace clang::ast_matchers;

				// clang-format off
				auto matcher = cxxRecordDecl(
					anyOf(
						// member (by-reference and by-value)
						hasMethod(cxxMethodDecl(
							hasName("operator<=>"),
							unless(isDeleted()),
							isPublic(),
							parameterCountIs(1), hasParameter(0,
								parmVarDecl(hasType(references(cxxRecordDecl(equalsNode(&decl))))))
						).bind("op")),
						hasMethod(cxxMethodDecl(
							hasName("operator<=>"),
							unless(isDeleted()),
							isPublic(),
							parameterCountIs(1), hasParameter(0,
								parmVarDecl(hasType(cxxRecordDecl(equalsNode(&decl)))))
						).bind("op")),
						
						// hidden friend (by-reference and by-value)
						has(friendDecl(has(functionDecl(
							hasName("operator<=>"),
							unless(isDeleted()),
							isPublic(),
							parameterCountIs(2),
							hasParameter(0, parmVarDecl(hasType(references(cxxRecordDecl(equalsNode(&decl)))))),
							hasParameter(1, parmVarDecl(hasType(references(cxxRecordDecl(equalsNode(&decl))))))
						).bind("op")))),
						has(friendDecl(has(functionDecl(
							hasName("operator<=>"),
							unless(isDeleted()),
							isPublic(),
							parameterCountIs(2),
							hasParameter(0, parmVarDecl(hasType(cxxRecordDecl(equalsNode(&decl))))),
							hasParameter(1, parmVarDecl(hasType(cxxRecordDecl(equalsNode(&decl)))))
						).bind("op")))),
						
						// free function (by-reference and by-value)
						hasAncestor(namespaceDecl(has( functionDecl(
							hasName("operator<=>"),
							unless(isDeleted()),
							parameterCountIs(2),
							hasParameter(0, parmVarDecl(hasType(references(cxxRecordDecl(equalsNode(&decl)))))),
							hasParameter(1, parmVarDecl(hasType(references(cxxRecordDecl(equalsNode(&decl))))))
						).bind("op")))),
						hasAncestor(namespaceDecl(has( functionDecl(
							hasName("operator<=>"),
							unless(isDeleted()),
							parameterCountIs(2),
							hasParameter(0, parmVarDecl(hasType(cxxRecordDecl(equalsNode(&decl))))),
							hasParameter(1, parmVarDecl(hasType(cxxRecordDecl(equalsNode(&decl)))))
						).bind("op"))))
						
					)
				);
				// clang-format on

				class MatchCallback final : public clang::ast_matchers::MatchFinder::MatchCallback {
				  public:
					std::optional<QualType> type;

					void run(const clang::ast_matchers::MatchFinder::MatchResult& r) override
					{
						if(auto f = r.Nodes.getNodeAs<FunctionDecl>("op")) {
							if(auto ff = f->getType()->getAs<clang::FunctionType>()) {
								type = ff->getReturnType();
							}
						}
					}
				};

				auto found  = MatchCallback{};
				auto finder = clang::ast_matchers::MatchFinder();
				finder.addMatcher(matcher, &found);
				finder.match(decl, decl.getASTContext());

				return found.type;
			}

			bool attribute_ignored(std::string_view str)
			{
				return str.starts_with("deprecated(") || str.starts_with("nodiscard(") || str == "noexcept"
				       || str == "nodiscard" || str == "final" || str == "override";
			}

			// Mask: Destructor, Default-C, Move-C, Move=, Copy-C, Copy=
			std::uint8_t detect_special_members(const clang::CXXRecordDecl& type)
			{
				auto special_member_functions = std::uint8_t(0);

				// clang-format off
				// detect copy constructor
				if(auto cc = std::find_if(type.ctor_begin(), type.ctor_end(),
										  [](auto* c) {return c->isCopyConstructor();}); cc!=type.ctor_end()) {
					special_member_functions |= (*cc)->isDeleted() ? 0 : 0b0010u;
				} else {
					special_member_functions |= type.hasSimpleCopyConstructor() ? 0b0010u : 0b0000u;
				}

				// detect move constructor
				if(auto cc = std::find_if(type.ctor_begin(), type.ctor_end(),
										  [](auto* c) {return c->isMoveConstructor();}); cc!=type.ctor_end()) {
					special_member_functions |= (*cc)->isDeleted() ? 0 : 0b1000u;
				} else {
					special_member_functions |= type.hasSimpleMoveConstructor() ? 0b1000u : 0b0000u;
				}

				// detect copy assignment
				if(auto mm = std::find_if(type.method_begin(), type.method_end(),
										  [](auto* m) {return m->isCopyAssignmentOperator();}); mm!=type.method_end()) {
					special_member_functions |= (*mm)->isDeleted() ? 0 : 0b0001u;
				} else {
					special_member_functions |= type.hasSimpleCopyAssignment() ? 0b0001u : 0b0000u;
				}

				// detect move assignment
				if(auto mm = std::find_if(type.method_begin(), type.method_end(),
										  [](auto* m) {return m->isMoveAssignmentOperator();}); mm!=type.method_end()) {
					special_member_functions |= (*mm)->isDeleted() ? 0 : 0b0100u;
				} else {
					special_member_functions |= type.hasSimpleMoveAssignment() ? 0b0100u : 0b0000u;
				}

				// detect default constructor
				if(auto cc = std::find_if(type.ctor_begin(), type.ctor_end(),
										  [](auto* c) {return (c->isDefaultConstructor() || c->getMinRequiredArguments()==0) && !c->isDeleted();}); cc!=type.ctor_end()) {
					special_member_functions |= 0b1'0000u;
				} else {
					special_member_functions |= type.hasDefaultConstructor() ? 0b1'0000u : 0;
				}

				// detect destructor
				if(auto destr = type.getDestructor(); type.hasSimpleDestructor() || (destr && !destr->isDeleted()) || (!destr && !type.defaultedDestructorIsDeleted())) {
					special_member_functions |= 0b10'0000u;
				}

				// clang-format on

				return special_member_functions;
			}

			std::set<std::string> extract_attributes(const clang::ASTContext& ctx, clang::NamedDecl& decl)
			{
				auto actual = &decl;

				auto attributes = std::set<std::string>();

				for(auto& a : actual->attrs()) {
					if(a->getAttrName()
					   && (a->getAttrName()->getName() == "visibility"
					       || a->getAttrName()->getName() == "dllexport")) {
						attributes.insert("DLL-Exported");
						continue;
					}

					auto str =
					        clang::Lexer::getSourceText(clang::CharSourceRange::getTokenRange(a->getRange()),
					                                    ctx.getSourceManager(),
					                                    ctx.getLangOpts())
					                .str();

					if(!str.empty() && !attribute_ignored(str))
						attributes.insert(std::move(str));
				}

				if(auto td = llvm::dyn_cast<clang::TypedefNameDecl>(actual)) {
					if(auto tag = td->getAnonDeclWithTypedefName();
					   tag && tag->isThisDeclarationADefinition()) {
						attributes.emplace(
						        ":abbr:`Typedef-Struct (C-Style struct defined as typedef struct {...} "
						        "Name)`");
					} else if(auto tag = td->getUnderlyingType()->getAsTagDecl()) {
						auto ud_record = llvm::dyn_cast<clang::CXXRecordDecl>(tag);
						if(ud_record && !ud_record->hasDefinition()
						   && !llvm::isa<clang::ClassTemplateSpecializationDecl>(tag))
							attributes.emplace(":abbr:`Opaque-Handle (Type alias for an undefined struct)`");
					}
				}
				if(auto aliases = db.reverse_lookup_aliases("struct " + ::ucpp::name(*actual));
				   !aliases.empty()) {
					attributes.emplace(
					        ":abbr:`Typedef-Struct (C-Style struct defined as typedef struct {...} "
					        "Name)`");
				}

				if(auto func = llvm::dyn_cast<clang::FunctionDecl>(actual)) {
					if(func->isExternC()) {
						attributes.emplace(":abbr:`C-Linkage (Marked as extern \"C\")`");
					}
					if(func->isConstexpr()) {
						attributes.emplace("Constexpr");
					} else if(func->isConsteval()) {
						attributes.emplace("Consteval");
					}
					if(func == func->getDefinition() && func->getLexicalParent() != func->getParent()) {
						attributes.emplace(":abbr:`Hidden-Friend`");
					}
				}

				if(auto var = llvm::dyn_cast<clang::VarDecl>(actual)) {
					if(var->isConstexpr()) {
						attributes.emplace("Constexpr");
					}
				}

				if(auto type = llvm::dyn_cast<clang::CXXRecordDecl>(actual)) {
					if(!type->getDefinition()) {
						attributes.emplace(
						        ":abbr:`Incomplete-Type (Type declaration without any visible definition)`");
						return attributes;
					}

					if(type->isPOD()) {
						attributes.emplace(":abbr:`POD (A Plain Old Data type that is compatible with C)`");
					} else {
						if(type->isAggregate()
						   && type->forallBases([](auto* base) { return base->isAggregate(); })) {
							attributes.emplace(
							        ":abbr:`Aggregate (Members can be initialized directly using T{...})`");
						}
						if(type->isTrivial()
						   && type->forallBases([](auto* base) { return base->isTrivial(); })) {
							attributes.emplace(":abbr:`Trivial (Is trivially copy-able and constructable)`");
						}
						if(type->isCXX11StandardLayout()) {
							attributes.emplace("Standard-Layout");
						}
					}

					auto special_member_functions = detect_special_members(*type);

					type->forallBases([&](auto* base) {
						special_member_functions &= detect_special_members(*base);
						return true;
					});

					if((special_member_functions & 0b10'0000) == 0) {
						attributes.emplace(":abbr:`Indestructible (Has no accessible destructor)`");

					} else if((special_member_functions & 0b0011) == 0b0011
					          && (special_member_functions & 0b1'0000)) {
						if(!has_equality_compare(*type)) {
							attributes.emplace(
							        ":abbr:`Semi-Regular (Can be copied, assigned and default "
							        "constructed, but "
							        "is not comparable)`");
						} else {
							attributes.emplace(
							        ":abbr:`Regular (Can be copied, assigned, default constructed and "
							        "compared using ==)`");
						}

					} else {
						switch(special_member_functions & 0b1111) {
							case 0b0000:
								attributes.emplace(
								        ":abbr:`Immovable (Has no accessible copy or move operations)`");
								break;

							case 0b1000:
								attributes.emplace(
								        ":abbr:`Move-Constructable (Has a move constructor but no "
								        "assignment "
								        "operator)`");
								break;

							case 0b0010:
								attributes.emplace(
								        ":abbr:`Copy-Constructable (Has a copy constructor but no "
								        "copy-assignment operator)`");
								break;

							case 0b1100:
								attributes.emplace(":abbr:`Move-only (Is move-able but can't be copied)`");
								break;

							case 0b0011:
							case 0b0111:
							case 0b1011:
							case 0b1111:
								attributes.emplace(":abbr:`Copyable (Can be copied and assigned)`");
								break;

							case 0b1010:
								attributes.emplace(
								        ":abbr:`Non-Assignable (Has a move/copy constructor but no "
								        "assignment operators)`");
								break;

							case 0b0101:
							case 0b0001:
								attributes.emplace(
								        ":abbr:`Only-Assignable (Has no move/copy constructors but "
								        "assignment operators)`");
								break;

							default: break;
						}
					}


					if(auto order_type = get_order_type(*type)) {
						const auto ot_str = order_type->getAsString(printing_policy);

						if(ot_str == "std::strong_ordering") {
							attributes.emplace("Ordered (strong)");
						} else if(ot_str == "std::weak_ordering") {
							attributes.emplace("Ordered (weak)");
						} else if(ot_str == "std::partial_ordering") {
							attributes.emplace("Ordered (partial)");
						} else {
							attributes.emplace("Ordered (unknown)");
						}
					}

					if(type->isAbstract()) {
						attributes.emplace(":abbr:`Abstract (Has at least one pure virtual function)`");
					}
					if(type->isPolymorphic()) {
						attributes.emplace(":abbr:`Polymorphic (Has a virtual functions)`");
					}

					if(db.is_hashable(::ucpp::name(*type))) {
						attributes.emplace(":abbr:`Hashable (Has a custom specialization of std::hash)`");
					}
					if(db.is_tuple_like(::ucpp::name(*type))) {
						attributes.emplace(
						        ":abbr:`Tuple-Like (Can be used in std::get and structured-bindings, like a "
						        "std::tuple)`");
					}
				}

				return attributes;
			}

			std::vector<std::string> extract_requirements(const clang::ASTContext& ctx,
			                                              clang::NamedDecl&        decl) const
			{
				auto requirements = std::vector<std::string>();

				if(auto templ = llvm::dyn_cast<clang::TemplateDecl>(&decl)) {
					llvm::SmallVector<const clang::Expr*, 4> constraints;
					templ->getAssociatedConstraints(constraints);
					for(auto* c : constraints) {
						requirements.push_back(to_string(*c));
					}
				}

				if(auto templ = decl.getDescribedTemplateParams()) {
					llvm::SmallVector<const clang::Expr*, 4> constraints;
					templ->getAssociatedConstraints(constraints);
					for(auto* c : constraints) {
						requirements.push_back(to_string(*c));
					}
				}

				if(auto func = llvm::dyn_cast<clang::DeclaratorDecl>(&decl)) {
					if(auto c = func->getTrailingRequiresClause()) {
						requirements.push_back(to_string(*c));
					}
				}

				if(auto conc = llvm::dyn_cast<clang::ConceptDecl>(&decl)) {
					if(auto expr = conc->getConstraintExpr()) {
						foreach_conjunction_term(expr, [&](auto& term) {
							if(!llvm::isa<clang::RequiresExpr>(term)) {
								requirements.push_back(to_string(*term));
							}
						});
					}
				}

				return requirements;
			}

			void write_comment_header(const clang::ASTContext& ctx, std::ostream& out, Entity e)
			{
				out << push;

				write_location(ctx, out, e);

				if(auto decl = std::get_if<clang::NamedDecl*>(&e)) {
					if(const auto attributes = extract_attributes(ctx, **decl); !attributes.empty()) {
						out << ".. admonition:: Attributes" << push << "\n";

						for(auto& a : attributes) {
							out << "\n - " << a;
						}

						out << pop << "\n\n";
					}

					if(const auto requirements = extract_requirements(ctx, **decl); !requirements.empty()) {
						out << ".. admonition:: Requirements" << push << "\n";

						for(auto& r : requirements) {
							out << "\n - :cpp:expr:`" << r << '`';
						}

						out << pop << "\n\n";
					}

					auto message = std::string();
					if((*decl)->isDeprecated(&message)) {
						out << ".. admonition:: Deprecated" << push << "\n\n" << message << pop << "\n\n";
					}
				}
			}

			void write_comment_footer() { out << pop << "\n\n"; }


			void write_comment(const clang::ASTContext& ctx, std::ostream& out, Entity e)
			{
				write_comment_header(ctx, out, e);

				// copy processed doc-comment to out, inlining special sections directly into out
				if(auto c = comment(e, &ctx); !c.empty()) {
					out << '\n';
					process_comment(ctx, out, e, c, 0, [&](auto&&...) { return nullptr; });
					out << '\n';
				}

				write_comment_footer();
			}

			template <typename V>
			V& find(std::vector<std::pair<std::string, V>>& c, std::string_view key, bool push_front = false)
			{
				auto iter = std::find_if(c.begin(), c.end(), [&](auto& e) { return key == e.first; });

				if(iter != c.end()) {
					return iter->second;
				} else if(push_front) {
					c.template emplace(c.begin(), key, V{});
					return c.front().second;
				} else {
					return c.template emplace_back(key, V{}).second;
				}
			}

			void write_comment(const clang::ASTContext& ctx, std::ostream& out, Entity_list& overloads)
			{
				constexpr auto overload_comment_sep = std::string_view("\\overload_specific");

				auto print_ids = [&](auto& ids) {
					if(ids.size() < overloads.size() && ids.size() > 0) {
						out << " [";
						auto first = true;
						for(auto id : ids) {
							if(first)
								first = false;
							else
								out << ',';
							out << id;
						}

						out << ']';
					}
				};

				// write locations for all overloads
				out << push;
				for(auto& of : overloads) {
					write_location(ctx, out, of);
				}

				// write attributes
				auto attributes = std::vector<std::pair<std::string, std::vector<int>>>();
				for(int id = 1; auto& of : overloads) {

					if(auto decl = std::get_if<clang::NamedDecl*>(&of)) {
						for(auto& a : extract_attributes(ctx, **decl)) {
							find(attributes, a).emplace_back(id);
						}
					}

					id++;
				}

				if(!attributes.empty()) {
					out << ".. admonition:: Attributes" << push << "\n";

					for(auto&& [a, ids] : attributes) {
						out << "\n - " << a;
						print_ids(ids);
					}

					out << pop << "\n\n";
				}

				// write requirements
				auto requirements = std::vector<std::pair<std::string, std::vector<int>>>();
				for(int id = 1; auto& of : overloads) {
					if(auto decl = std::get_if<clang::NamedDecl*>(&of)) {
						for(auto& a : extract_requirements(ctx, **decl)) {
							find(requirements, a).emplace_back(id);
						}
					}

					id++;
				}
				if(!requirements.empty()) {
					out << ".. admonition:: Requirements" << push << "\n";

					for(auto&& [r, ids] : requirements) {
						out << "\n - :cpp:expr:`" << r << '`';
						print_ids(ids);
					}

					out << pop << "\n\n";
				}

				// write deprecation notices
				for(int i = 0; auto& o : overloads) {
					i++;
					if(auto decl = std::get_if<clang::NamedDecl*>(&o)) {
						auto message = std::string();
						if((*decl)->isDeprecated(&message)) {
							out << ".. admonition:: Deprecated" << push << "\n\n"
							    << message << " [" << i << "]" << pop << "\n\n";
						}
					}
				}

				// parse comment strings
				// section_title -> overload_nr -> content
				auto comments = std::vector<std::pair<std::string, std::map<int, Indentation_sstream>>>();
				comments.emplace_back();

				auto common_comment_entity = &overloads[0];
				auto common_comment        = std::string();

				for(int id = 1; auto& of : overloads) {
					if(auto c = comment(of, &ctx); !c.empty()) {
						auto comment = std::string_view{c};
						auto offset  = 0;

						if(auto sep = comment.find(overload_comment_sep); sep != std::string::npos) {
							if(sep > common_comment.size()) {
								if(!common_comment.empty()) {
									std::cerr << "Overload set has differing prefixes on the comments: "
									          << ::ucpp::id(*std::get<clang::NamedDecl*>(of)) << '\n';
								}
								common_comment        = comment.substr(0, sep);
								common_comment_entity = &of;
								offset                = sep;
							}

							comment = comment.substr(sep + overload_comment_sep.size());
						} else if(common_comment.empty()) {
							// if we haven't found a common prefix yet, we keep all of it
							common_comment        = comment;
							common_comment_entity = &of;
							continue;
						}

						process_comment(ctx,
						                comments[0].second[id],
						                of,
						                comment,
						                offset,
						                [&](auto&&... section_name_parts) {
							                auto str = std::stringstream{};
							                (str << ... << section_name_parts);
							                return &find(comments, std::move(str).str())[id];
						                });
					}

					id++;
				}

				// comment body shared by all overloads
				auto comments_size_pre = comments.size();
				process_comment(ctx,
				                out,
				                *common_comment_entity,
				                common_comment,
				                0,
				                [&](auto&&... section_name_parts) {
					                auto str = std::stringstream{};
					                (str << ... << section_name_parts);
					                return &find(comments, std::move(str).str(), true)[0];
				                });
				const auto comment_body_offset = comments.size() - comments_size_pre;

				if(const auto& bodies = comments[comment_body_offset].second; bodies.size() >= 1) {
					// per overload comments
					out << "\n\n";

					for(auto id = 1; id <= static_cast<int>(overloads.size()); id++) {
						if(auto comment = bodies.find(id); comment != bodies.end()) {
							auto str = std::move(comment->second).str();
							trim(str);
							if(!str.empty()) {
								out << id << ". " << indent::push(3) << str << indent::pop(3) << "\n\n";
								continue;
							}
						}
					}
				}

				for(auto&& [key, sections] : comments) {
					if(key.empty())
						continue;

					auto sections_compiled = std::vector<std::pair<std::string, std::vector<int>>>();
					for(auto&& [id, ss] : sections) {
						auto  str = trimmed(std::move(ss).str());
						auto& ids = (str.empty() && !sections_compiled.empty())
						                    ? sections_compiled[0].second
						                    : find(sections_compiled, str);

						if(id != 0)
							ids.emplace_back(id);
					}

					for(auto&& [content, ids] : sections_compiled) {
						out << '\n' << key << push << content;
						print_ids(ids);
						out << pop << "\n";
					}
				}

				out << pop << "\n\n";
			}

			std::string name(const clang::NamedDecl* d, std::string_view scope)
			{
				if(auto spec = llvm::dyn_cast<clang::ClassTemplateSpecializationDecl>(d)) {
					if(auto type = spec->getTypeAsWritten()) {
						return type->getType().getAsString(printing_policy);
					}
				}

				auto qname      = ::ucpp::name(*d);
				auto local_name = remove_common_scope(qname, scope);

				// skip pseudo identifier of outer types
				while(local_name.starts_with("(anonymous ")) {
					auto i = local_name.find(')');
					if(i == std::string::npos)
						break;

					if(i < local_name.size() - 3 && local_name[i + 1] == ':' && local_name[i + 2] == ':')
						local_name = local_name.substr(i + 3);
					else
						local_name = local_name.substr(i + 1);
				}

				if(local_name.empty())
					return "@" + std::to_string(reinterpret_cast<std::intptr_t>(d));
				else if(local_name.ends_with("(anonymous)"))
					return std::string(local_name.substr(0, local_name.size() - 11)) + "@"
					       + std::to_string(reinterpret_cast<std::intptr_t>(d));
				else
					return std::string{local_name};
			}

			static std::string template_header(const clang::NamedDecl* d)
			{
				std::string s;

				if(auto templ = llvm::dyn_cast<clang::TemplateDecl>(d)) {
					llvm::raw_string_ostream ss(s);
					templ->getTemplateParameters()->print(ss, d->getASTContext());
					ss.str();

				} else if(auto t = d->getDescribedTemplateParams()) {
					llvm::raw_string_ostream ss(s);
					t->print(ss, d->getASTContext());
					ss.str();

				} else if(llvm::isa<clang::ClassTemplateSpecializationDecl>(*d)) {
					return "template<> ";

				} else {
					return s;
				}

				if(s.empty())
					s = "template<> ";

				return s;
			}

			void write_macro(const Macro& m)
			{
				out << ".. c:macro:: " << m.identifier;

				if(m.info->isFunctionLike()) {
					out << '(';

					auto first = true;
					for(auto& p : m.info->params()) {
						if(first)
							first = false;
						else
							out << ',';

						if(p->getName() == "__VA_ARGS__")
							out << "...";
						else
							out << p->getName();
					}

					out << ')';
				}

				out << "\n\n";
				out << push;

				if(!m.info->tokens_empty()) {
					const auto def = clang::Lexer::getSourceText(clang::CharSourceRange::getTokenRange(
					                                                     {m.info->getDefinitionLoc(),
					                                                      m.info->getDefinitionEndLoc()}),
					                                             m.ctx->getSourceManager(),
					                                             m.ctx->getLangOpts())
					                         .str();

					if(def.size() < 32) {
						out << ".. code-block:: cpp" << push << "\n\n#define " << def << pop << "\n\n";
					}
				}

				write_location(*m.ctx, out, &m);

				if(auto c = comment(&m); !c.empty()) {
					process_comment(*m.ctx, out, &m, c, 0, [&](auto&&...) { return nullptr; });
				}

				out << pop << "\n\n";
			}

			bool isScopeVisible(const clang::DeclContext* dc) const override
			{
				return std::any_of(scopes.begin(), scopes.end(), [&](auto& s) {
					return s.context == dc || s.context->Encloses(dc);
				});
			}

			static void remove_keyword(std::string& str, std::string_view keyword)
			{
				std::size_t i = 0;
				while((i = str.find(keyword, i)) != std::string::npos) {
					if(i == 0 || isspace(str[i - 1])) {
						str.erase(i, keyword.size());
					} else {
						i += keyword.size();
					}
				}
			}

			std::string to_string(clang::Decl& decl)
			{
				std::string              s;
				llvm::raw_string_ostream ss(s);
				printing_policy.FullyQualifiedName = scopes.empty();
				//if(decl.getAsFunction())
				//	printing_policy.PrintCanonicalTypes = true;
				decl.print(ss, printing_policy);
				ss.str();

				auto fd = decl.getAsFunction();

				if(fd && fd == fd->getDefinition() && fd->getLexicalParent() != fd->getParent()) {
					// hidden friend
					s = "friend " + s;
				}

				// workaround for broken pretty-printing of conditional noexcept clauses in libtooling
				if(auto ft = decl.getFunctionType()) {
					if(auto fpt = llvm::dyn_cast<clang::FunctionProtoType>(ft)) {
						if(fpt->getNoexceptExpr()) {
							auto begin         = s.find("noexcept(");
							auto actual_clause = s.find("noexcept(", begin + 1);

							if(begin != std::string::npos && actual_clause != std::string::npos) {
								s.erase(begin, actual_clause - begin);
							}
						}
					}
				}

				// strip generalized attributes
				std::size_t index = 0;
				while((index = s.find("[[", index)) != std::string::npos) {
					auto end = s.find("]]", index);
					if(end != std::string::npos) {
						s.erase(index, end + 2 - index);
					} else {
						index = end;
					}
				}

				auto skip_args = [&] {
					const auto begin = index;

					index = s.find('(', index);
					if(index == std::string::npos) {
						return;
					}

					if(auto non_space = s.find_first_not_of(" \n\t\r", begin); non_space < index) {
						return;
					}

					auto parens = 0;

					for(; index < s.size(); ++index) {
						switch(s[index]) {
							case '"':
								index++;
								for(; index < s.size() && s[index] != '"'; ++index) {
									if(s[index] == '\\')
										index += 2;
								}
								break;

							case '(': parens++; break;
							case ')': parens--; break;
						}

						if(parens == 0) {
							break;
						}
					}
					index++;
				};

				// strip __attributes__
				index = 0;
				while((index = s.find("__attribute__", index)) != std::string::npos) {
					const auto begin = index;
					index += 13;
					skip_args();
					s.erase(begin, index - begin);
					index = begin;
				}

				if(llvm::isa<clang::CXXMethodDecl>(decl)) {
					// sphinx is currently confused by member functions with requires clauses, so we have to remove them (for now)
					index = 0;
					while((index = s.find(" requires", index)) != std::string::npos) {
						const auto begin = index;
						index += 9;
						if(s[index] != ' ' && s[index] != '(')
							continue;

						skip_args();
						s.erase(begin, index - begin);
						index = begin;
					}
				}

				remove_keyword(s, "constexpr ");
				remove_keyword(s, "inline ");

				if(fd) {
					// convert trailing return type to normal one
					if(auto trailing = s.rfind("->"); trailing != std::string::npos) {
						if(auto placeholder = s.find("auto"); placeholder != std::string::npos) {
							auto return_type = trimmed(s.substr(trailing + 2));
							s.erase(trailing);
							s.replace(placeholder, 4, return_type);
						}
					}

					// improvement for return type deduction
					auto pp_deduced                = printing_policy;
					pp_deduced.PrintCanonicalTypes = true;
					auto decl_ret                  = fd->getDeclaredReturnType().getAsString(printing_policy);
					auto clang_deduced_ret         = fd->getReturnType().getAsString(printing_policy);
					auto deduced_ret               = fd->getReturnType().getAsString(pp_deduced);
					if(decl_ret != deduced_ret && decl_ret.ends_with(" auto")) {
						// if this is a function that uses a concept-restricted deduced return type, print the concept name instead of the deduction
						if(auto i = s.find(clang_deduced_ret); i != std::string::npos) {
							s.replace(i, clang_deduced_ret.size(), decl_ret);
						}
					} else if(clang_deduced_ret != deduced_ret
					          && (clang_deduced_ret.starts_with("auto")
					              || clang_deduced_ret.starts_with("decltype"))
					          && deduced_ret.find("-") == std::string::npos) {
						// if deduced_ret is not a "weird" internal name, replace the declared return type with the deduced one
						if(auto i = s.find(clang_deduced_ret); i != std::string::npos) {
							s.replace(i, clang_deduced_ret.size(), deduced_ret);
						}
					}
				}

				return s;
			}
			static std::string_view to_string(clang::TagTypeKind k)
			{
				switch(k) {
					case clang::TTK_Struct: return "struct";
					case clang::TTK_Interface: return "interface";
					case clang::TTK_Union: return "union";
					case clang::TTK_Class: return "class";
					case clang::TTK_Enum: return "enum";
				}
				return "<unknown>";
			}
			std::string to_string(const clang::Expr& expr) const
			{
				std::string              s;
				llvm::raw_string_ostream ss(s);
				expr.printPretty(ss, nullptr, printing_policy);
				ss.str();

				if(s.size() > 2 && s.front() == '(' && s.back() == ')') {
					s.erase(0, 1);
					s.pop_back();
				}

				return s;
			}

			template <typename WriteDef>
			void write_entity(clang::NamedDecl* e, std::string_view type, WriteDef&& write_def)
			{
				out << ".. cpp:" << type << ":: " << template_header(e) << name(e, this->current_scope);
				write_def();
				out << "\n\n";
				write_comment(e->getASTContext(), out, e);
			}

			void post_visit()
			{
				if(last_written_scope) {
					out << ".. cpp:namespace-pop:: \n\n" << pop;
					last_written_scope.reset();
				}
				current_scope = {};
			}

			void pre_entity(clang::NamedDecl& decl)
			{
				if(!namespace_scopes.empty()
				   && last_written_scope != namespace_scopes.back().qualified_name) {
					if(last_written_scope) {
						out << ".. cpp:namespace-pop:: \n\n" << pop;
					}

					last_written_scope = namespace_scopes.back().qualified_name;

					out << ".. cpp:namespace-push:: " << *last_written_scope << "\n\n";
					out << ".. rst-class:: simple\n\n";
					out << ".. cpp:ucpp_namespace:: " << *last_written_scope << "\n\n";

					out << push;
				}

				if(!access.empty() && !access.back().written) {
					out << ".. rst-class:: k\n\n";
					out << ".. rubric:: " << getAccessSpelling(access.back().specifier) << ":\n\n";
					access.back().written = true;
				}
			}

			template <typename T, typename F>
			auto with_scope(T* decl, bool ns, F&& f)
			{
				auto name = ::ucpp::name(*decl);
				scopes.push_back(Scope{decl, name});
				current_scope = name + "::";
				if(ns)
					namespace_scopes.push_back(Scope{decl, name});

				auto r = f();

				scopes.pop_back();
				current_scope = scopes.empty() ? "" : scopes.back().qualified_name + "::";

				if(ns)
					namespace_scopes.pop_back();

				return r;
			}

			bool TraverseNamespaceDecl(clang::NamespaceDecl* decl)
			{
				if(decl->isAnonymousNamespace() || decl->getName() == "detail" || decl->getName() == "details"
				   || db.is_excluded(*decl))
					return true;

				return with_scope(
				        decl, true, [&] { return RecursiveASTVisitor::TraverseNamespaceDecl(decl); });
			}

			void pre_class(clang::RecordDecl* decl)
			{
				clang::NamedDecl* actual = decl;

				if(const clang::TagDecl* tag = llvm::dyn_cast<clang::TagDecl>(decl)) {
					if(auto a = tag->getTypedefNameForAnonDecl()) {
						actual = a;
					}
				}

				pre_entity(*actual);

				out << ".. cpp:" << to_string(decl->getTagKind()) << ":: " << template_header(actual)
				    << name(actual, this->current_scope);

				if(auto cxx = llvm::dyn_cast<clang::CXXRecordDecl>(decl); cxx && cxx->getDefinition()) {
					if(cxx->isEffectivelyFinal())
						out << " final";

					if(cxx->bases_begin() != cxx->bases_end()) {
						auto first = true;
						out << " : ";
						for(auto& base : cxx->bases()) {
							if(first)
								first = false;
							else
								out << ", ";

							out << getAccessSpelling(base.getAccessSpecifier()) << ' ';
							if(base.isVirtual())
								out << "virtual ";
							out << remove_common_scope(base.getType().getAsString(printing_policy),
							                           current_scope);
						}
					}
				}

				out << "\n\n";
				write_comment(actual->getASTContext(), out, actual);

				out << push;

				auto new_access = decl->getAccess();
				if(new_access == clang::AS_none) {
					new_access =
					        decl->getTagKind() == clang::TTK_Class ? clang::AS_private : clang::AS_public;
				}

				access.push_back(Access{new_access});
			}
			bool TraverseRecordDecl(clang::RecordDecl* decl)
			{
				if(!db.is_excluded(*decl)) {
					if(!decl->hasNameForLinkage() && !decl->isAnonymousStructOrUnion()) {
						std::cout << "skipped " << ::ucpp::name(*decl) << '\n';
						return true; // member of anonymous type, e.g. union {...} xxx => handled by VisitVarDecl()
					}

					pre_class(decl);

					return with_scope(decl, false, [&] {
						auto r = RecursiveASTVisitor::TraverseRecordDecl(decl);
						access.pop_back();
						out << pop << '\n';
						return r;
					});

				} else {
					return true;
				}
			}
			bool TraverseCXXRecordDecl(clang::CXXRecordDecl* decl)
			{
				if(!db.is_excluded(*decl)) {
					if(!decl->hasNameForLinkage() && !decl->isAnonymousStructOrUnion()) {
						return true; // member of anonymous type, e.g. union {...} xxx => handled by VisitVarDecl()
					}

					pre_class(decl);

					return with_scope(decl, false, [&] {
						auto r = RecursiveASTVisitor::TraverseRecordDecl(decl);
						access.pop_back();
						out << pop << '\n';
						return r;
					});

				} else {
					return true;
				}
			}
			bool VisitAccessSpecDecl(clang::AccessSpecDecl* decl)
			{
				assert(!access.empty());
				if(!access.empty() && access.back().specifier != decl->getAccess())
					access.back() = Access{decl->getAccess()};
				return true;
			}

			bool TraverseClassTemplateSpecializationDecl(clang::ClassTemplateSpecializationDecl* decl)
			{
				if(!db.is_excluded(*decl)) {
					pre_class(decl);

					return with_scope(decl, false, [&] {
						auto r = RecursiveASTVisitor::TraverseClassTemplateSpecializationDecl(decl);
						access.pop_back();
						out << pop << '\n';
						return r;
					});

				} else {
					return true;
				}
			}
			bool TraverseClassTemplatePartialSpecializationDecl(
			        clang::ClassTemplatePartialSpecializationDecl* decl)
			{
				if(!db.is_excluded(*decl)) {
					pre_class(decl);

					return with_scope(decl, false, [&] {
						auto r = RecursiveASTVisitor::TraverseClassTemplatePartialSpecializationDecl(decl);
						access.pop_back();
						out << pop << '\n';
						return r;
					});

				} else {
					return true;
				}
			}
			bool TraverseVarTemplateSpecializationDecl(clang::VarTemplateSpecializationDecl*) { return true; }
			bool TraverseVarTemplatePartialSpecializationDecl(clang::VarTemplatePartialSpecializationDecl*)
			{
				return true;
			}

			std::string arg(clang::ConceptSpecializationExpr* constraint, std::size_t n)
			{
				std::string              s;
				llvm::raw_string_ostream ss(s);
#ifdef UCPP_LLVM_PRE_13
				(*constraint->getTemplateArgsAsWritten())[n].getArgument().print(printing_policy, ss);
#else
				(*constraint->getTemplateArgsAsWritten())[n].getArgument().print(printing_policy, ss, true);
#endif
				ss.str();
				return s;
			}

			bool VisitConceptDecl(clang::ConceptDecl* decl)
			{
				if(!db.is_excluded(*decl)) {
					pre_entity(*decl);

					out << ".. cpp:concept:: " << template_header(decl) << name(decl, this->current_scope)
					    << "\n\n";

					write_comment_header(decl->getASTContext(), out, decl);

					auto main_comment   = Indentation_sstream();
					auto param_comments = std::unordered_map<std::string, Indentation_sstream>();

					// copy processed doc-comment to out, inlining special sections directly into out
					if(auto c = comment(decl, &decl->getASTContext()); !c.empty()) {
						process_comment(decl->getASTContext(),
						                main_comment,
						                decl,
						                c,
						                0,
						                overloaded{[&](const char*      key,
						                               std::string_view field_name,
						                               const char*) -> std::ostream* {
							                           if(key == std::string_view(":rparam ")) {
								                           return &param_comments[std::string(field_name)];
							                           } else {
								                           return nullptr;
							                           }
						                           },
						                           [](auto&&...) { return nullptr; }});
					}

					out << "\n\n";

					if(auto expr = decl->getConstraintExpr()) {
						foreach_conjunction_term(expr, [&](auto& term) {
							if(auto req = llvm::dyn_cast<clang::RequiresExpr>(term)) {
								out << "**Let**\n\n" << push;
								for(auto* param : req->getLocalParameters()) {
									out << ".. cpp:var:: " << to_string(*param) << "\n\n";

									if(auto iter = param_comments.find(param->getNameAsString());
									   iter != param_comments.end()) {
										out << push << iter->second.str() << pop << "\n\n";
									}
								}

								out << pop;
								out << "**Requirements**\n\n" << push;

								for(auto* requirement : req->getRequirements()) {
									if(auto expr_req = llvm::dyn_cast<clang::concepts::ExprRequirement>(
									           requirement)) {
										out << "- :cpp:expr:`" << to_string(*expr_req->getExpr()) << '`';

										if(expr_req->getReturnTypeRequirement().isTypeConstraint()) {
											auto ret_req =
											        expr_req->getReturnTypeRequirement().getTypeConstraint();

											if(auto* constraint =
											           llvm::dyn_cast<clang::ConceptSpecializationExpr>(
											                   ret_req->getImmediatelyDeclaredConstraint())) {
												if(auto* concept_decl = constraint->getFoundDecl()) {
													auto concept_name =
													        concept_decl->getQualifiedNameAsString();

													if(concept_name == "std::same_as") {
														out << ", evaluates to a :cpp:expr:`"
														    << arg(constraint, 1) << '`';

													} else if(concept_name == "std::derived_from") {
														out << ", evaluates to a type derived from "
														       ":cpp:expr:`"
														    << arg(constraint, 1) << '`';

													} else if(concept_name == "std::convertible_to") {
														out << ", evaluates to a type convertible to "
														       ":cpp:expr:`"
														    << arg(constraint, 1) << '`';

													} else if(concept_name == "std::integral") {
														out << ", evaluates to an integer type";

													} else if(concept_name == "std::signed_integral") {
														out << ", evaluates to an signed integer type";

													} else if(concept_name == "std::unsigned_integral") {
														out << ", evaluates to an unsigned integer type";

													} else if(concept_name == "std::floating_point") {
														out << ", evaluates to an floating point type";

													} else if(concept_name == "std::swappable") {
														out << ", evaluates to a swappable type";

													} else if(concept_name == "std::destructible") {
														out << ", evaluates to a destructible type";

													} else if(concept_name == "std::default_initializable") {
														out << ", evaluates to a default constructable "
														       "type";

													} else if(concept_name == "std::move_constructible") {
														out << ", evaluates to a move-constructable type";

													} else if(concept_name == "std::copy_constructible") {
														out << ", evaluates to a copy-constructable type";

													} else if(concept_name == "std::equality_comparable") {
														out << ", evaluates to a equality comparable "
														       "type";

													} else if(concept_name == "std::totally_ordered") {
														out << ", evaluates to a totally ordered type";

													} else if(concept_name == "std::movable") {
														out << ", evaluates to a movable type";

													} else if(concept_name == "std::copyable") {
														out << ", evaluates to a copyable type";

													} else if(concept_name == "std::semiregular") {
														out << ", evaluates to a semi-regular type";

													} else if(concept_name == "std::regular") {
														out << ", evaluates to a regular type";

													} else if(concept_name == "std::predicate") {
														out << ", evaluates to a predicate";

													} else if(concept_name == "std::relation") {
														out << ", evaluates to a binary relation";

													} else if(concept_name == "std::equivalence_relation") {
														out << ", evaluates to a equivalence relation";

													} else if(concept_name == "std::strict_weak_order") {
														out << ", evaluates to a strict weak ordering";

													} else
														out << ", satisfies concept :cpp:expr:`"
														    << to_string(*constraint) << '`';
												}

											} else {
												std::string              s;
												llvm::raw_string_ostream ss(s);
												ret_req->print(ss, printing_policy);
												ss.str();

												out << ", satisfies :cpp:expr:`" << s << '`';
											}
										} else {
											out << ", is a valid expression";
										}

										out << "\n\n";
									}
								}

								out << pop;
							}
						});
					}

					out << '\n' << main_comment.str() << '\n';

					write_comment_footer();
				}
				return true;
			}
			bool VisitTypedefDecl(clang::TypedefDecl* decl)
			{
				if(!db.is_excluded(*decl)) {
					// skip typedef from "typedef struct {} XXX", that is already handled by the struct visitor
					if(const clang::TagDecl* tag = decl->getUnderlyingType()->getAsTagDecl()) {
						if(tag->getTypedefNameForAnonDecl()) {
							return true;
						}
					}

					pre_entity(*decl);
					write_entity(decl, "type", [&] {
						out << " = " << decl->getUnderlyingType().getAsString(printing_policy);
					});
				}

				return true;
			}
			bool VisitTypeAliasDecl(clang::TypeAliasDecl* decl)
			{
				if(!db.is_excluded(*decl)) {
					pre_entity(*decl);
					write_entity(decl, "type", [&] {
						out << " = " << decl->getUnderlyingType().getAsString(printing_policy);
					});
				}
				return true;
			}
			bool VisitEnumDecl(clang::EnumDecl* decl)
			{
				if(!db.is_excluded(*decl)) {
					pre_entity(*decl);
					write_entity(decl, decl->isScoped() ? "enum-class" : "enum", [&] {
						if(decl->getIntegerTypeSourceInfo()) {
							out << " : " << decl->getIntegerType().getAsString(printing_policy);
						}
					});

					if(decl->enumerator_begin() != decl->enumerator_end()) {
						out << push << "\n\n";
						for(auto&& e : decl->enumerators()) {
							out << ".. cpp:enumerator:: " << to_string(*e) << "\n\n";
							write_comment(decl->getASTContext(), out, e);
						}
						out << pop;
					}
				}
				return true;
			}
			bool VisitFunctionDecl(clang::FunctionDecl* decl)
			{
				if(!db.is_excluded(*decl, true)) {
					const auto decl_id = id(*decl);

					if(!written_functions.emplace(decl_id).second)
						return true;

					pre_entity(*decl);

					auto overloads = db.lookup(Entity_type::function_t, decl_id);

					if(overloads.size() > 1) {
						out << ".. cpp:function:: " << push << push;
						for(auto& of : overloads) {
							std::visit(overloaded{[&](clang::NamedDecl* decl) {
								                      out << template_header(decl) << to_string(*decl)
								                          << '\n';
							                      },
							                      [](auto&&) {}},
							           of);
						}
						out << pop << pop << "\n";

						write_comment(decl->getASTContext(), out, overloads);

					} else {
						out << ".. cpp:function:: " << template_header(decl) << to_string(*decl) << "\n\n";
						write_comment(decl->getASTContext(), out, decl);
					}
				}
				return true;
			}
			bool VisitFieldDecl(clang::FieldDecl* decl)
			{
				if(!db.is_excluded(*decl)) {
					pre_entity(*decl);

					if(auto r = decl->getType()->getAsRecordDecl()) {
						if(!r->getDeclName() && !r->isAnonymousStructOrUnion()) {
							// member of anonymous type, e.g. union {...} xxx
							// printed as a .. cpp:var:: union @42342 xxx
							//   followed by the types definition
							out << ".. cpp:var:: " << to_string(r->getTagKind()) << " "
							    << name(r, this->current_scope) << " " << decl->getNameAsString() << "\n\n";
							write_comment(decl->getASTContext(), out, decl);

							auto new_access = r->getAccess();
							if(new_access == clang::AS_none) {
								new_access = r->getTagKind() == clang::TTK_Class ? clang::AS_private
								                                                 : clang::AS_public;
							}

							access.push_back(Access{new_access});

							out << push << '\n';
							return with_scope(r, false, [&] {
								auto res = RecursiveASTVisitor::TraverseRecordDecl(r);
								access.pop_back();
								out << pop << '\n';
								return res;
							});
						}
					}

					out << ".. cpp:var:: " << template_header(decl) << to_string(*decl) << "\n\n";
					write_comment(decl->getASTContext(), out, decl);
				}
				return true;
			}

			bool VisitVarDecl(clang::VarDecl* decl)
			{
				if(!db.is_excluded(*decl) && !decl->isLocalVarDeclOrParm()) {
					pre_entity(*decl);

					out << ".. cpp:var:: " << template_header(decl) << to_string(*decl) << "\n\n";
					write_comment(decl->getASTContext(), out, decl);
				}
				return true;
			}
		};

	} // namespace

	bool write_markup(std::ostream& out, const Parser_db& db, const Entity_list& entities)
	{
		auto visitor = Visitor{out, db};

		for(auto& e : entities) {
			std::visit(overloaded{[&](const File* f) {
				                      out << "**#include <" << db.to_relative_path(f->path).generic_string()
				                          << ">**\n\n";
				                      visitor.write_comment(f->root->getASTContext(), out, e);

				                      for(auto& m : f->macros)
					                      visitor.write_macro(*m);

				                      visitor.TraverseDecl(f->root);
			                      },
			                      [&](const Macro* m) { visitor.write_macro(*m); },
			                      [&](clang::NamedDecl* d) { visitor.TraverseDecl(d); }},
			           e);
		}

		visitor.post_visit();

		return true;
	}

	void write_example(std::ostream& out, clang::FunctionDecl& decl)
	{
		auto printing_policy               = default_printing_policy();
		printing_policy.FullyQualifiedName = true;

		auto& sm = decl.getASTContext().getSourceManager();

		const auto& body                      = *decl.getBody();
		const auto [begin_file, begin_offset] = sm.getDecomposedLoc(body.getBeginLoc());
		const auto [end_file, end_offset]     = sm.getDecomposedLoc(body.getEndLoc());

		if(begin_file != end_file) {
			std::cout << "Internal error in Parser_db::get_example: begin and end of function body are "
			             "in different files: "
			          << body.getBeginLoc().printToString(sm) << " and " << body.getEndLoc().printToString(sm)
			          << '\n';
			return;
		}

		const auto data   = sm.getCharacterData(body.getBeginLoc());
		const auto length = end_offset - begin_offset;

		auto code = std::string(data + 1, length - 1);
		remove_redundant_indentation(code);

		out << ".. code-block:: cpp\n" << push;
		out << ":linenos:\n\n";

		if(!decl.param_empty()) {
			out << "// Let:\n";
			for(auto&& param : decl.parameters()) {
				out << param->getType().getAsString(printing_policy) << " " << param->getNameAsString()
				    << " = ...;\n";
			}
		}

		out << code << pop << "\n\n";
	}

} // namespace ucpp
