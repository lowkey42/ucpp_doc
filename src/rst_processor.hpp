#pragma once

#include <filesystem>

namespace cppast {
	class diagnostic_logger;
}

namespace ucpp {

	class Parser_db;

	extern bool process_rst_files(const Parser_db&             db,
	                              const std::filesystem::path& input,
	                              const std::filesystem::path& output);

} // namespace ucpp
