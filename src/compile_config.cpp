#include "compile_config.hpp"

#include <process.hpp>

#include <cstring>
#include <iostream>
#include <vector>

namespace tpl = TinyProcessLib;

namespace ucpp {

	namespace {
		bool is_valid_binary(const std::string& binary)
		{
			tpl::Process process(
			        binary + " -v", "", [](const char*, std::size_t) {}, [](const char*, std::size_t) {});
			return process.get_exit_status() == 0;
		}

#if(defined(WIN32) || defined(_WIN32) || defined(__WIN32)) && !defined(__CYGWIN__)
#define UCPP_DETAIL_WINDOWS 1
#else
#define UCPP_DETAIL_WINDOWS 0
#endif

	} // namespace

	compile_config::compile_config()
	  : write_preprocessed_(false)
	  , fast_preprocessing_(false)
	  , remove_comments_in_macro_(false)
	{
		// set given clang binary
		set_clang_binary(UCPP_DOC_CLANG_BINARY);
	}

	void compile_config::add_default_include_dirs()
	{
		std::string  verbose_output;
		tpl::Process process(
		        clang_binary_ + " -x c++ -v -",
		        "",
		        [](const char*, std::size_t) {},
		        [&](const char* str, std::size_t n) { verbose_output.append(str, n); },
		        true);
		process.write("", 1);
		process.close_stdin();
		process.get_exit_status();

		auto pos = verbose_output.find("#include <...>");
		if(pos == std::string::npos) {
			std::cerr << "Unexpected output from clang: " << verbose_output << "\n";
			std::abort();
		}
		while(verbose_output[pos] != '\n')
			++pos;
		++pos;

		// now every line is an include path, starting with a space
		while(verbose_output[pos] == ' ') {
			auto start = pos + 1;
			while(verbose_output[pos] != '\r' && verbose_output[pos] != '\n')
				++pos;
			auto end = pos;
			++pos;

			auto        line = verbose_output.substr(start, end - start);
			std::string path;
			for(auto c : line) {
				if(c == ' ') { // escape spaces
#if UCPP_DETAIL_WINDOWS
					path += "^ ";
#else
					path += "\\ ";
#endif
				}
				// clang under MacOS adds comments to paths using '(' at the end, so they have to be
				// ignored however, Windows uses '(' in paths, so they don't have to be ignored
#if !UCPP_DETAIL_WINDOWS
				else if(c == '(')
					break;
#endif
				else
					path += c;
			}

			add_include_dir(std::move(path));
		}
	}

	bool compile_config::set_clang_binary(std::string binary)
	{
		if(is_valid_binary(binary)) {
			clang_binary_ = binary;
			add_default_include_dirs();
			return true;
		} else {
			// first search in current directory, then in PATH
			static const char* paths[] = {"./clang++",
			                              "clang++",
			                              "./clang++-4.0",
			                              "clang++-4.0",
			                              "./clang++-5.0",
			                              "clang++-5.0",
			                              "./clang++-6.0",
			                              "clang++-6.0",
			                              "./clang-7",
			                              "clang-7",
			                              "./clang-8",
			                              "clang-8",
			                              "./clang-9",
			                              "clang-9",
			                              "./clang-10",
			                              "clang-10",
			                              "./clang-11",
			                              "clang-11"};
			for(auto& p : paths)
				if(is_valid_binary(p)) {
					clang_binary_ = p;
					add_default_include_dirs();
					return false;
				}

			throw std::invalid_argument("unable to find clang binary '" + binary + "'");
		}
	}

	void compile_config::set_flags(cpp_standard standard)
	{
		add_flag(std::string("-std=") + to_string(standard));
	}

	bool compile_config::enable_feature(std::string name)
	{
		add_flag("-f" + std::move(name));
		return true;
	}

	void compile_config::add_include_dir(std::string path) { add_flag("-I" + std::move(path)); }

	void compile_config::define_macro(std::string name, std::string definition)
	{
		auto str = "-D" + std::move(name);
		if(!definition.empty())
			str += "=" + definition;
		add_flag(std::move(str));
	}

	void compile_config::undefine_macro(std::string name)
	{
		add_flag("-U" + std::move(name));
	}

	void compile_config::get_arguments(std::vector<const char*>& args) const
	{
		args.push_back("-x");
		args.push_back("c++");
		args.push_back("-I.");
		
		for(auto& flag : flags_)
			args.push_back(flag.c_str());
	}


} // namespace ucpp
