#pragma once

#include "cpp_parser.hpp"

namespace ucpp {

	extern bool write_markup(std::ostream&, const Parser_db& db, const Entity_list& entities);

	extern void write_example(std::ostream&, clang::FunctionDecl&);

} // namespace ucpp
