#include "rst_processor.hpp"

#include "cpp_parser.hpp"
#include "rst_writer.hpp"
#include "util.hpp"

#include <boost/iostreams/device/mapped_file.hpp>

#include <algorithm>
#include <fstream>
#include <iostream>
#include <iterator>
#include <optional>
#include <tuple>

namespace ucpp {

	namespace {

		constexpr auto markup_end = std::string_view(":\n ");

		constexpr auto markup          = std::string_view(".. ucpp:");
		const auto     markup_searcher = std::boyer_moore_horspool_searcher(markup.begin(), markup.end());

		constexpr auto min_file_size = markup.size() + 4;

		constexpr auto max_files_to_delete = 256;


		std::optional<Entity_type> parse_entity_type(std::string_view str)
		{
			// clang-format off
		if(     "file"      == str) return Entity_type::file_t;
		else if("macro"     == str) return Entity_type::macro_t;
		else if("type"      == str) return Entity_type::type_t;
		else if("variable"  == str) return Entity_type::variable_t;
		else if("function"  == str) return Entity_type::function_t;
		else if("concept"   == str) return Entity_type::concept_t;
		else                        return std::nullopt;
			// clang-format on
		}

		auto find_column(const char* begin, const char* curr)
		{
			const auto last_line_break =
			        std::find(std::make_reverse_iterator(curr), std::make_reverse_iterator(begin), '\n');
			return static_cast<int>(curr - last_line_break.base());
		}

		auto find_file_position(const char* begin, const char* curr)
		{
			const auto last_line_break =
			        std::find(std::make_reverse_iterator(curr), std::make_reverse_iterator(begin), '\n');
			const auto column = curr - last_line_break.base();

			const auto line = 1 + std::count(begin, last_line_break.base(), '\n');

			return std::tuple{static_cast<int>(line), static_cast<int>(column)};
		}

		std::string_view trim(std::string_view str)
		{
			size_t first = str.find_first_not_of(' ');
			if(first == std::string::npos)
				return "";
			size_t last = str.find_last_not_of(' ');
			return str.substr(first, (last - first + 1));
		}


		bool process(const Parser_db& db, const std::filesystem::path& in, const std::filesystem::path& out)
		{
			std::cout << "Processing markup file " << in.generic_string() << ":1 into " << out.generic_string()  << ":1 \n";

			auto       in_stream = boost::iostreams::mapped_file_source{in};
			auto       last_p    = in_stream.begin();
			const auto end       = in_stream.end();

			auto out_stream = std::optional<std::ofstream>();

			auto error_at = [&](const char* p) -> std::ostream& {
				auto [line, column] = find_file_position(in_stream.begin(), p);
				std::cerr << "Error in " << in << ':' << line << ":" << column << ": ";
				return std::cerr;
			};

			auto p = std::search(last_p, end, markup_searcher);

			for(; p != end; p = std::search(p, end, markup_searcher)) {
				const auto indent = find_column(last_p, p);

				if(!out_stream) {
					out_stream = std::ofstream(out);
				}

				// copy the text we haven't processed
				if(p - indent > last_p) {
					const auto dist = p - indent - last_p;
					out_stream->write(last_p, dist);
				}

				// enable indentation for the remaining output operations
				auto indent_iomanip = Indentation(*out_stream, indent);

				p += markup.length();

				const auto separator = std::find_first_of(p, end, markup_end.begin(), markup_end.end());
				if(separator == end || *separator != ':') {
					error_at(separator) << "Expected ':' but found '" << *separator << "'\n";
					return false;
				}

				const auto eol = std::find(separator, end, '\n');
				const auto ref = trim(std::string_view(separator + 2, eol));

				if(std::string_view(p, separator) == "include_example") {
					if(auto example = db.get_example(std::string(ref))) {
						write_example(*out_stream, *example);
					} else {
						error_at(p) << "Reference to unknown example '" << ref << "'\n";
						*out_stream << ".. ERROR::\n";
						*out_stream << "  Reference to unknown example '" << ref << "' in '" << in << "'\n";
					}
				
				} else {
					const auto type = parse_entity_type(std::string_view(p, separator));

					if(!type) {
						error_at(separator)
						        << "Unknown reference type '" << std::string_view(p, separator) << "'\n";
						return false;
					}

					const auto entities = db.lookup(*type, ref);
					if(entities.empty()) {
						error_at(p) << "Reference to unknown entity '" << ref << "'\n";
						*out_stream << ".. ERROR::\n";
						*out_stream << "  Reference to unknown entity '" << ref << "' in '" << in << "'\n";

					} else if(!write_markup(*out_stream, db, entities)) {
						error_at(p) << "Failed to resolve reference to '" << ref << "' in '" << in << "'\n";
						*out_stream << ".. ERROR::\n";
						*out_stream << "  Failed to resolve reference to '" << ref << "' in '" << in << "'\n";
					}
				}

				p = eol;

				last_p = p;
			}

			if(out_stream) {
				out_stream->write(last_p, end - last_p);
				return true;
			} else {
				return false;
			}
		}

		bool clean_output_directory(const std::filesystem::path& input, const std::filesystem::path& output)
		{
			using namespace std::filesystem;

			auto paths_to_delete = std::vector<std::filesystem::path>();
			for(auto& p : recursive_directory_iterator(output)) {
				const auto in = input / relative(p, output);
				if(!exists(in) || status(p).type() != status(in).type()) {
					paths_to_delete.emplace_back(p);
				}
			}

			if(paths_to_delete.size() > max_files_to_delete) {
				std::cerr << "The cleanup operation would delete more than " << max_files_to_delete
				          << " from the output directory '" << output
				          << "'. This operation has been cancled for safety reasons. If you are sure please "
				             "delete this directory manually!\n";
				return false;
			}

			for(auto& p : paths_to_delete) {
				(void) remove_all(p);
			}
			return true;
		}
	} // namespace

	bool process_rst_files(const Parser_db&             db,
	                       const std::filesystem::path& input,
	                       const std::filesystem::path& output)
	{
		using namespace std::filesystem;

		if(!is_directory(output) && !create_directories(output)) {
			std::cerr << "Couldn't create output root directory '" << output << "'\n";
			return false;
		}

		if(!clean_output_directory(input, output))
			return false;

		for(auto& p : recursive_directory_iterator(input)) {
			const auto out = output / relative(p, input);

			if(p.is_regular_file()) {
				const auto dir = out.parent_path();
				if(!exists(dir) && !create_directories(dir)) {
					std::cerr << "Couldn't create directory for output file '" << out << "'\n";
					return false;
				}

				if(p.path().extension() != ".rst" || p.file_size() < min_file_size || !process(db, p, out)) {
					copy(p, out, copy_options::update_existing | copy_options::copy_symlinks);
				}

			} else if(p.is_directory()) {
				create_directories(out);
			}
		}

		auto extension_path = output / "ucpp.py";
		if(!exists(extension_path)) {
			auto out = std::ofstream{extension_path};
			out << R"xxx(
import re
import sphinx

from typing import TYPE_CHECKING, Any, Dict, List, Tuple, Union, cast

import regex
from html import unescape

from sphinx import addnodes
from sphinx.application import Sphinx
from sphinx.directives import ObjectDescription
from sphinx.directives import optional_int
from sphinx.directives.code import CodeBlock
from docutils.parsers.rst import directives
from docutils.statemachine import StringList
from docutils.nodes import document
from sphinx.locale import _

from docutils import nodes
from docutils.nodes import Element, Node, TextElement, paragraph

from sphinx.writers.html import HTMLTranslator
from sphinx.writers.html5 import HTML5Translator
from sphinx.util.docutils import is_html5_writer_available

from sphinx.domains.cpp import ASTDeclaratorPtr, ASTDeclaratorRef, CPPExprRole

from sphinx.util.cfamily import verify_description_mode

from sphinx.util.nodes import find_pending_xref_condition

from sphinx.util import logging, parselinenos
from sphinx.util.docutils import SphinxDirective
from sphinx.util.typing import OptionSpec

from sphinx.builders.html import StandaloneHTMLBuilder

from sphinx.parsers import RSTParser

class UcppNamespaceDirective(ObjectDescription):
    has_content = True
    def handle_signature(self, sig, signode):
        signode += addnodes.desc_sig_keyword('namespace', 'namespace')
        signode += addnodes.desc_sig_space()
        signode += addnodes.desc_name(text=sig)
        return sig

def my_find_pending_xref_condition(node, conditions):
    for condition in conditions:
        matched = find_pending_xref_condition(node, condition)
        if matched:
            return matched.children
    else:
        return None

def resolve_ext_reference(app: Sphinx, target):
    i = target.find('<')
    if i>0:
        target = target[0:i]

    for prefix, url in app.config.ucpp_ext_mapping.items():
        if target.startswith(prefix):
            return ((target.rsplit('::', 1)[-1] or target) if prefix[-1]==':' else target, url.format(target=target))
    return (None, None)

def solve_pending_xref(app: Sphinx, node: addnodes.pending_xref):
    (dispname, uri) = resolve_ext_reference(app, node['reftarget'])
    if uri:
        newnode = nodes.reference('', '', internal=False, refuri=uri, reftitle=node['reftarget'])

        content = my_find_pending_xref_condition(node, ("resolved", "*"))
        if content:
            contnode = cast(Element, content[0].deepcopy())
        else:
            contnode = cast(Element, node[0].deepcopy())

        if node.get('refexplicit'):
            # use whatever title was given
            newnode.append(contnode)
        else:
            # else use the given display name (used for :ref:)
            newnode.append(contnode.__class__(dispname, dispname))
        return newnode
    else:
        return None


def doctree_read(app: Sphinx, doctree: Node) -> None:
    env = app.builder.env

    for node in doctree.traverse(addnodes.pending_xref):
        newnode = solve_pending_xref(app, node)
        if newnode:
            node.replace_self(newnode)

    for line in doctree.traverse(addnodes.desc_signature_line):
        if line.sphinx_line_type == 'templateParams':
            for sub in line.traverse(addnodes.desc_sig_element):
                sub['classes'].append('template_header')

    resolve_target = getattr(env.config, 'ucpp_resolve', None)
    if callable(env.config.ucpp_resolve):
        for objnode in doctree.traverse(addnodes.desc):
            locations = []
            for n in objnode.traverse(include_self=False):
                if isinstance(n, addnodes.desc):
                    break

                if not isinstance(n, paragraph):
                    continue

                matches = re.findall('\[ucpp\_src\_location: *([^;]+); *([0-9]+)\]', n.astext(), re.IGNORECASE)
                if matches:
                    locations += matches
                    n.clear()
                    
            if locations:
                index = 0
                for signode in objnode:
                    if isinstance(signode, addnodes.desc_signature) and index<len(locations):
                        (filename, line) = locations[index]
                        index += 1
                        url = resolve_target(filename, line)
                        if url:
                            inline = nodes.inline('', _('[src]'), classes=['viewcode-link'])
                            onlynode = addnodes.only(expr='html')
                            onlynode += nodes.reference('', '', inline, internal=False, refuri=url)
                            signode += onlynode

class UcppExprTypeInfo(SphinxDirective):
    has_content = False
    required_arguments = 1
    optional_arguments = 1
    final_argument_whitespace = True
    option_spec = {}  # type: Dict

    def warn(self, msg):
        # type: (unicode) -> None
        self.state_machine.reporter.warning(msg, line=self.lineno)

    def run(self):
        # type: () -> List[nodes.Node]
        if 'var_context' not in self.env.domaindata['cpp']:
            self.env.domaindata['cpp']['var_context'] = {}

        if len(self.arguments) > 1:
            self.env.domaindata['cpp']['var_context'][self.arguments[1].strip()] = self.arguments[0].strip()
        else:
            self.env.domaindata['cpp']['var_context'][''] = self.arguments[0].strip()
        return []

class UcppExprRole(CPPExprRole):
    def __call__(self, typ, rawtext, text, lineno, inliner, options={}, content=[]):
        r = super().__call__(typ, rawtext, text, lineno, inliner, options, content)

        env = inliner.document.settings.env

        if 'var_context' in env.domaindata['cpp']:
            varCtx = env.domaindata['cpp']['var_context']

            varNode = None
            isMember = True
            for node in r[0][0]:
                if isinstance(node, addnodes.pending_xref):
                    varNode = node
                elif isinstance(node, addnodes.desc_sig_name) and varNode:
                    newnode = varNode.copy()
                    newnode['reftarget'] = node.rawsource
                    if varNode['reftarget'] in varCtx:
                         newnode['reftarget'] = varCtx[varNode['reftarget']] + "::"+ newnode['reftarget']

                    newnode += node.deepcopy()
                    node.parent.replace(node, newnode)
        return r;

identifier_pattern = regex.compile(r'(?:<span class=\"n\">(?P<var>\w[\w\d_]*)<\/span>(?P<var_sep>\s*<span class=\"(?:p|o)\">(?:\.|-&gt;)<\/span>))<span class=\"n\">(?P<id>\w[\w\d_]*)<\/span>|(?:<span class=\"n\">(?P<ns>\w[\w\d_]*)<\/span>(?P<ns_sep>\s*<span class=\"o\">::<\/span>\s*))*<span class=\"n\">(?P<id>\w[\w\d_]*)<\/span>')

class UcppCodeBlock(CodeBlock):
    highlighter = None

    def create_ref_node(self, lang, id, name):
        ref_node = addnodes.pending_xref('', refdomain=lang,
                                         reftype='identifier',
                                         reftarget=id, modname=None,
                                         classname=None)
        
        if 'cpp:parent_key' in self.env.ref_context:
        	ref_node['cpp:parent_key'] = self.env.ref_context['cpp:parent_key']
        
        ref_node += addnodes.desc_name(name, name)
        
        signode = nodes.inline('')
        signode += ref_node
        return signode

    def run(self) -> List[Node]:
        literal = super().run()[0]

        if not UcppCodeBlock.highlighter:
            b = StandaloneHTMLBuilder(self.env.app)
            b.init()
            UcppCodeBlock.highlighter = b.highlighter
            
        if UcppCodeBlock.highlighter:
            lang = literal.get('language', 'default')
            linenos = literal.get('linenos', False)
            highlight_args = literal.get('highlight_args', {})
            highlight_args['force'] = True
            opts = self.config.highlight_options.get(lang, {})
    
            if linenos and self.config.html_codeblock_linenos_style:
                linenos = self.config.html_codeblock_linenos_style
    
            highlighted = UcppCodeBlock.highlighter.highlight_block(
                literal.rawsource, lang, opts=opts, linenos=linenos,
                location=literal, **highlight_args
            )

            html = '<div class="highlight-%s notranslate">' % lang + highlighted + '</div>\n'
            last_end = 0

            varCtx = None
            ctxHasWildcard = False
            if 'var_context' in self.env.domaindata['cpp']:
                varCtx = self.env.domaindata['cpp']['var_context']
                ctxHasWildcard = '' in varCtx

            node_list = []

            for m in identifier_pattern.finditer(html):
                start, end = m.span()
                if last_end<start:
                    node_list.append(nodes.raw('', html[last_end:start], format = 'html'))

                last_end=end

                name = m.group("id")
                id = name

                var_name = m.group("var");

                if var_name is not None:
                    if varCtx is not None and var_name in varCtx:
                        id = varCtx[var_name] + "::" + name

                    node_list.append(nodes.raw('', html[start:m.start("id")], format = 'html'))
                    
                else:
                    namespaces = m.captures("ns")
                    namespace_spans = m.spans("ns_sep")
                    id = '::'.join(namespaces + [name])
                    for i in range(len(namespaces)):
                        node_list.append(self.create_ref_node(lang, '::'.join(namespaces[0:i+1]), namespaces[i]))
                        node_list.append(nodes.raw('', html[namespace_spans[i][0]:namespace_spans[i][1]], format = 'html'))

                    if len(namespaces)==0 and varCtx is not None:
                        var_access = html[start-8 : start] == '.</span>'
                        ptr_access = html[start-9 : start] == '-></span>'
                        if var_access or ptr_access:
                            expr_end = start-9 if ptr_access else start-8
                            expr_start = html.rfind('<span class="linenos">', 0, expr_end)
                            if expr_start > -1:
                                expr_start = html.find('</span>', expr_start) + len('</span>')
                                for s in range(expr_end-1, expr_start, -1):
                                    text = unescape(regex.sub('<[^<]+?>', '', html[s:expr_end]))
                                    if ctxHasWildcard and (text=='  ' or text=='\t'):
                                        id = varCtx[''] + "::" + name
                                        break
                                    if text in varCtx:
                                        id = varCtx[text] + "::" + name
                                        break
                        
                node_list.append(self.create_ref_node(lang, id, name))

            if last_end<len(html):
                node_list.append(nodes.raw('', html[last_end:], format = 'html'))

            return node_list

        else:
            return [literal]

class PatchedHTMLTranslator(
    HTML5Translator if is_html5_writer_available() else HTMLTranslator
):
    def starttag(self, node, tagname, *args, **attrs):
        if (
            tagname == "a"
            and "target" not in attrs
            and (
                "external" in attrs.get("class", "")
                or "external" in attrs.get("classes", [])
            )
        ):
            attrs["target"] = "_blank"
            attrs["ref"] = "noopener noreferrer"

        return super().starttag(node, tagname, *args, **attrs)

# workaround for "misplaced" ref/ptr-qualifiers in sphinx: https://github.com/sphinx-doc/sphinx/issues/7491
def no_spaces(self):
    return False

def describe_ptr_signature(self, signode: TextElement, mode: str,
                           env: "BuildEnvironment", symbol: "Symbol") -> None:
    verify_description_mode(mode)
    signode += addnodes.desc_sig_punctuation('*', '*')
    for a in self.attrs.attrs:
        a.describe_signature(signode)
    if len(self.attrs) > 0 and (self.volatile or self.const):
        signode += addnodes.desc_sig_space()
    def _add_anno(signode: TextElement, text: str) -> None:
        signode += addnodes.desc_sig_keyword(text, text)
    if self.volatile:
        _add_anno(signode, 'volatile')
    if self.const:
        if self.volatile:
            signode += addnodes.desc_sig_space()
        _add_anno(signode, 'const')
    if self.next.require_space_after_declSpecs():
        signode += addnodes.desc_sig_space()
    self.next.describe_signature(signode, mode, env, symbol)

def describe_ref_signature(self, signode: TextElement, mode: str,
                           env: "BuildEnvironment", symbol: "Symbol") -> None:
    verify_description_mode(mode)
    signode += addnodes.desc_sig_punctuation('&', '&')
    for a in self.attrs.attrs:
        a.describe_signature(signode)
    if self.next.require_space_after_declSpecs():
        signode += addnodes.desc_sig_space()
    self.next.describe_signature(signode, mode, env, symbol)

class NoTabExpansionRSTParser(RSTParser):
    def parse(self, inputstring: Union[str, StringList], document: document) -> None:
        if isinstance(inputstring, str):
            lines = inputstring.splitlines()
            inputstring = StringList(lines, document.current_source)
        super().parse(inputstring, document)

def setup(app):
    app.add_config_value('ucpp_ext_mapping', {'std::': 'https://duckduckgo.com/?q=\\{target} site:en.cppreference.com'}, True)

    app.add_source_parser(NoTabExpansionRSTParser, override=True)

    ASTDeclaratorPtr.require_space_after_declSpecs = no_spaces
    ASTDeclaratorPtr.describe_signature = describe_ptr_signature
    ASTDeclaratorRef.require_space_after_declSpecs = no_spaces
    ASTDeclaratorRef.describe_signature = describe_ref_signature

    app.add_directive_to_domain('cpp', 'ucpp_namespace', UcppNamespaceDirective)
    app.add_directive_to_domain('cpp', 'ucpp_var_ctx', UcppExprTypeInfo)
    app.add_role_to_domain('cpp', 'expr', UcppExprRole(True), override=True)
    app.add_directive('code-block', UcppCodeBlock, override=True)

    app.connect('doctree-read', doctree_read)
    app.add_config_value('ucpp_resolve', None, '')

    app.set_translator("html", PatchedHTMLTranslator)
    
    app.add_css_file('css/ucpp.css')
    app.add_js_file('js/ucpp.js')

    return {
        'version': '0.1',
        'parallel_read_safe': True,
        'parallel_write_safe': True,
    }
)xxx";
		}

		auto css_path = output / "_static/css/ucpp.css";
		if(!exists(css_path)) {
			create_directories(css_path.parent_path());
			auto out = std::ofstream{css_path};
			out << R"xxx(
.function {
	counter-reset: overload;
}
/* add counter; this should only match functions with more than one signature */
.function > .sig + .sig:before,
.function > .sig:nth-last-child(n+3):before {
	counter-increment: overload;
	content: counter(overload) ". ";
	color: #666 !important;
}
:not(.ucpp_namespace) > .sig {
	text-indent: 0;
}

a.headerlink + br {
	display:none;
}
.function > .sig span.sig-paren ~ br {
	display:none;
}

.viewcode-link {
	padding: 0 !important;
}

.hidden-li {
	visibility: hidden;
	height: 0;
}

.template_header {
	font-size: 70%;
}

abbr[title] {
	text-decoration: none;
}

.api_list_type {
	min-width: 3em;
	display: inline-block;
	font-weight: bold;
}
.api_list {
	opacity: 60%;
}
.api_list ul {
	overflow: hidden;
}
.api_list li {
	display: inline-block;
	width: 100em;
}
.toc-scroll {
	overflow-x: hidden;
}

)xxx";
		}

		auto js_path = output / "_static/js/ucpp.js";
		if(!exists(js_path)) {
			create_directories(js_path.parent_path());
			auto out = std::ofstream{js_path};
			out << R"xxx(
function add_api_toc(parent, toc_parent, name_prefix) {
	var last_name = null;
	var count = 0;

	parent.querySelectorAll(':scope > dl > dt').forEach((member) => {
		if(member.parentNode.parentNode != parent)
			return;

		if(member.closest('.hide-toc')!=null)
			return;

		console.log(member);
		let nameNode = member.querySelector('.descname:not(:has(.template_header))');
		if(!nameNode)
			return;

		let name = nameNode.textContent;

		if(member.parentNode.classList.contains('ucpp_namespace')) {
			// inline namespaces
			count += add_api_toc(member.parentNode.getElementsByTagName('dd')[0], toc_parent, name_prefix+name+'::');

		} else {
			if(name=='' || last_name==name)
				return; // skip subsequent overloads
			last_name = name;

			var type = 'type';
			if(member.parentNode.classList.contains('var')) type = "var";
			else if(member.parentNode.classList.contains('function')) type = "func";
			else if(member.parentNode.classList.contains('macro')) type = "#define";

			var tocMember = document.createElement('li');
			//tocMember.textContent = member.querySelector('.property').textContent + ' ';

			var link = document.createElement('a');
			link.classList.add("reference");
			link.classList.add("internal");
			link.title = 'API for '+name_prefix + name;
			link.href = '#'+member.id;
			link.innerHTML = '<span class="api_list_type">'+type+' </span>'+name;
			tocMember.appendChild(link);
			toc_parent.appendChild(tocMember);
			count++;

			var skipChildren = member.parentNode.classList.contains('enum-class') || member.parentNode.classList.contains('enum');
			if(!skipChildren) {
				let members = document.createElement('ul');
				members.classList.add("api_list");
				toc_parent.appendChild(members);

				var child_count = add_api_toc(member.parentNode.getElementsByTagName('dd')[0], members, name_prefix+name+'::');
				if(child_count==0)
					toc_parent.removeChild(toc_parent.lastChild);
				count += child_count;
			}
		}
	});

	return count;
}

document.addEventListener("DOMContentLoaded", (event) => {
    document.querySelectorAll('section').forEach((section) => {
		if(section.closest('.hide-toc'))
			return;

        let toc_root = document.querySelector('.toc-tree li:has(> a.reference.internal[href="#'+section.id +'"])');
		if(!toc_root)
			return;

		console.log('root '+toc_root+'   '+section.id);
		let members = document.createElement('ul');
		members.classList.add("api_list");
		toc_root.appendChild(members);
        add_api_toc(section, members, '');
    });
});
)xxx";
		}

		return true;
	}

} // namespace ucpp
