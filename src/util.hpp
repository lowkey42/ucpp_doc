#pragma once

#include <algorithm>
#include <filesystem>
#include <iostream>
#include <string>

namespace cppast {
	class diagnostic_logger;
}

namespace ucpp {

	template <class... Ts>
	struct overloaded : Ts... {
		using Ts::operator()...;
	};
	template <class... Ts>
	overloaded(Ts...) -> overloaded<Ts...>;
	
	
	class Indentation : private std::streambuf {
	  public:
		explicit Indentation(std::ostream&, int indentation = 0);
		~Indentation();

		Indentation(Indentation&&)      = delete;
		Indentation(const Indentation&) = delete;

		Indentation& operator=(Indentation&&) = delete;
		Indentation& operator=(const Indentation&) = delete;

	  private:
		std::ostream&   stream_;
		std::streambuf& wrapped_;
		bool            indentation_pending_ = true;

		int overflow(int ch) override;
	};

	class Indentation_sstream : public std::stringstream {
	  public:
	  	Indentation_sstream() :  _indent(std::make_unique<Indentation>(*this, 0)) {}

	  private:
		std::unique_ptr<Indentation> _indent;
	};

	namespace detail {
		extern const int indent_index;

		struct indent_op {
			const int            width;
			friend std::ostream& operator<<(std::ostream& lhs, const indent_op& rhs)
			{
				lhs.iword(indent_index) += rhs.width;
				return lhs;
			}
		};
	} // namespace detail

	namespace indent {
		inline constexpr detail::indent_op push(int width) { return {width}; }
		inline constexpr detail::indent_op pop(int width) { return {-width}; }
	} // namespace indent

	inline void ltrim(std::string& s)
	{
		s.erase(s.begin(),
		        std::find_if(s.begin(), s.end(), [](unsigned char ch) { return !std::isspace(ch); }));
	}
	inline void rtrim(std::string& s)
	{
		s.erase(std::find_if(s.rbegin(), s.rend(), [](unsigned char ch) { return !std::isspace(ch); }).base(),
		        s.end());
	}
	inline void trim(std::string& s)
	{
		ltrim(s);
		rtrim(s);
	}
	
	inline std::string trimmed(std::string s)
	{
		ltrim(s);
		rtrim(s);
		return s;
	}

	std::size_t calculate_redundant_indentation(std::string_view str);
	std::size_t remove_redundant_indentation(std::string& str);

	inline bool contains(std::string_view str, std::string_view search) {
		return str.find(search) != std::string_view::npos;
	}

} // namespace ucpp
