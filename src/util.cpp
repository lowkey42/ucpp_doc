#include "util.hpp"

namespace ucpp {

	static constexpr std::string_view spaces =
	        "                                                               ";

	namespace detail {
		const int indent_index = std::ios_base::xalloc();
	}

	Indentation::Indentation(std::ostream& stream, int indentation)
	  : stream_(stream), wrapped_(*stream.rdbuf())
	{
		stream.rdbuf(this);
		stream.iword(detail::indent_index) = indentation;
	}
	Indentation::~Indentation()
	{
		stream_.rdbuf(&wrapped_);
	}

	int Indentation::overflow(int c)
	{
		if(indentation_pending_ && c != EOF && c != '\n') {
			auto indent = stream_.iword(detail::indent_index);

			for(std::size_t i = 0; i < indent / spaces.length(); ++i) {
				wrapped_.sputn(spaces.data(), spaces.length());
			}

			wrapped_.sputn(spaces.data(), indent % spaces.length());
		}
		indentation_pending_ = c == '\n';
		return wrapped_.sputc(c);
	}

	std::size_t calculate_redundant_indentation(std::string_view str) {
		auto min_indent = str.find_first_not_of(" \t");
		if(min_indent == 0 && str.size() > 1 && str[0] == '\n')
			min_indent = std::string::npos;

		// determine min indentation
		for(std::size_t i = 0; i != std::string::npos;) {
			auto newline = str.find('\n', i);
			if(newline == std::string::npos)
				break;
			if(newline + 1 < str.size() && str[newline + 1] == '\n') {
				i = newline + 1;
				continue;
			}

			auto first_char = str.find_first_not_of(" \t", newline + 1);
			if(first_char != std::string::npos)
				min_indent = std::min(min_indent, first_char - newline - 1);

			i = first_char;
		}

		return min_indent;
	}
	std::size_t remove_redundant_indentation(std::string& str)
	{
		const auto min_indent = calculate_redundant_indentation(str);

		// remove redundant indentation
		if(min_indent > 0 && min_indent != std::string::npos) {
			for(std::size_t i = str.size(); i != std::string::npos && i > 0;) {
				i = str.rfind('\n', i - 1);
				if(i != std::string::npos && i + 1 < str.size() && str[i + 1] != '\n') {
					if(auto count = std::min(min_indent, str.size() - i - 1); count > 0) {
						str.erase(i + 1, count);
					}
				}
			}
		}

		return min_indent;
	}

} // namespace ucpp
