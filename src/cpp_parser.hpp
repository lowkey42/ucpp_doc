#pragma once

#include "compile_config.hpp"

#include <clang/Basic/SourceLocation.h>

#include <tsl/htrie_map.h>

#include <filesystem>
#include <optional>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <variant>
#include <vector>

namespace clang {
	class ASTUnit;
	class Decl;
	class NamedDecl;
	class MacroInfo;
	class ASTContext;
	class PrintingPolicy;
	class CXXRecordDecl;
	class FunctionDecl;
	class RawComment;
} // namespace clang

namespace ucpp {

	enum class Entity_type {
		file_t,
		macro_t,
		type_t, // maps to class, struct, enum, enum class and aliases
		variable_t,
		function_t, // also maps to operators and constructors
		concept_t
	};

	struct Macro {
		std::string              identifier;
		const clang::MacroInfo*  info;
		const clang::RawComment* comment;
		const clang::ASTContext* ctx;
	};
	struct File {
		std::string                         path;
		std::unique_ptr<clang::ASTUnit>     unit;
		clang::Decl*                        root;
		std::vector<std::unique_ptr<Macro>> macros;
	};

	using Entity      = std::variant<clang::NamedDecl*, const File*, const Macro*>;
	using Entity_list = std::vector<Entity>;

	extern clang::PrintingPolicy default_printing_policy();

	extern std::string name(const clang::NamedDecl&);
	extern std::string id(const clang::NamedDecl&);
	extern std::string comment(const Entity&                    e,
	                           const clang::ASTContext*         ctx           = nullptr,
	                           std::vector<clang::PresumedLoc>* out_line_locs = nullptr);
	extern std::string comment(const clang::Decl&               e,
	                           const clang::ASTContext*         ctx           = nullptr,
	                           std::vector<clang::PresumedLoc>* out_line_locs = nullptr);
	extern bool        comment_empty(const clang::Decl& e);

	class Parser_db {
	  public:
		Parser_db();
		Parser_db(const Parser_db&)            = delete;
		Parser_db(Parser_db&&)                 = default;
		Parser_db& operator=(const Parser_db&) = delete;
		Parser_db& operator=(Parser_db&&)      = default;
		~Parser_db();

		bool is_excluded(const clang::NamedDecl&, bool keep_out_of_line = false) const;

		static Parser_db parse(compile_config& config, const std::vector<std::string>& files);

		/**
	   * Lookup an entity-reference found in the documentation.
	   *
	   * A reference can either be a short or qualified name (e.g. Entity_type, ucp::Entity_type or ::ucp::Entity_type)
	   * but not fully qualified names my match multiple entries.
	   *
	   * Function and template parameters may not be provided, i.e. functions are only referenced as complete overload-sets.
	   *
	   * Template specializations are merged into one entity and can't be referenced directly.
	   *
	   * Member functions and variables have to specify their class, e.g. Parser_db::lookup
	   *
	   * @return all matching entities
	   */
		Entity_list lookup(Entity_type type, std::string_view reference_string) const;

		std::filesystem::path to_relative_path(const std::filesystem::path& path) const;

		void register_entity(Entity_type type, Entity);

		void set_hashable(std::string qualified_name) { hashable_.emplace(std::move(qualified_name)); }
		bool is_hashable(const std::string& qualified_name) const
		{
			return hashable_.contains(qualified_name);
		}

		void set_tuple_like(std::string qualified_name) { tuple_like_.emplace(std::move(qualified_name)); }
		bool is_tuple_like(const std::string& qualified_name) const
		{
			return tuple_like_.contains(qualified_name);
		}

		void                 register_example(const std::string& name, clang::FunctionDecl*);
		clang::FunctionDecl* get_example(const std::string& name) const;

		const std::vector<std::string>& reverse_lookup_aliases(const std::string qualified_name) const
		{
			if(auto it = name_to_aliases_.find(qualified_name); it != name_to_aliases_.end()) {
				return it->second;
			} else {
				static const std::vector<std::string> empty;
				return empty;
			}
		}

	  private:
		std::vector<File> files_; ///< required to keep parse result alive

		/// trie of reversed fully qualified names, per type of entity.
		/// the ids are stored in reversed form, so the more general namespaces come later and can be optionally left out
		/// more detailed matching might be needed to distinguish all entities with the same name (e.g. overloaded functions).
		std::unordered_map<Entity_type, tsl::htrie_map<char, Entity_list>> entities_;

		std::vector<std::filesystem::path> base_directories_;

		std::unordered_set<std::string> hashable_;
		std::unordered_set<std::string> tuple_like_;

		std::unordered_map<std::string, std::vector<std::string>> name_to_aliases_;

		std::unordered_map<std::string, clang::FunctionDecl*> named_examples_;

		bool finalized_ = false;

		void do_parse();
	};


} // namespace ucpp
