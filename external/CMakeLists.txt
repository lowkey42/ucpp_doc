cmake_minimum_required(VERSION 3.14 FATAL_ERROR)

include(FetchContent)

add_subdirectory(hat-trie)

message(STATUS "Fetching cxxopts")
FetchContent_Declare(cxxopts URL https://github.com/jarro2783/cxxopts/archive/v2.2.1.zip DOWNLOAD_EXTRACT_TIMESTAMP true)
FetchContent_MakeAvailable(cxxopts)

message(STATUS "Fetching tiny-process-library")
FetchContent_Declare(tpl URL https://gitlab.com/eidheim/tiny-process-library/-/archive/master/tiny-process-library-master.zip DOWNLOAD_EXTRACT_TIMESTAMP true)
FetchContent_MakeAvailable(tpl)
