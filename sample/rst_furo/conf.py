
# required to use ucpp
import os
import sys
sys.path.insert(0, os.path.abspath('.'))

extensions = ['ucpp']
html_static_path = ['_static']

# called to lookup the url for a given source file
def ucpp_resolve(file, line):
    path = os.path.relpath(file, os.path.abspath('../cpp'))
    return "https://gitlab.com/lowkey42/ucpp_doc/-/blob/master/sample/cpp/"+path+"#L"+line

# used to link identifiers to external projects based on a prefix (either a namespace or a C-Style prefix)
# if the option is not specified, it only includes the definition for std::
ucpp_ext_mapping = {
    'std::' : 'https://duckduckgo.com/?q=\\{target} site:en.cppreference.com',
    'glfw' : 'https://duckduckgo.com/?q=\\{target} site:glfw.org/docs'
}

# -- Example project information -----------------------------------------------------

project = 'UCPP'
copyright = '2021, Florian Oetke'
author = 'Florian Oetke'

templates_path = []
exclude_patterns = ["_static"]

default_role = 'cpp:expr'
pygments_style = 'sphinx'
highlight_language = 'c++'
primary_domain = 'cpp'

html_theme = 'furo'
pygments_style = "sphinx"
pygments_dark_style = "monokai"

html_css_files = ['css/custom.css']
