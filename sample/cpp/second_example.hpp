#pragma once

#include "example.hpp"

#define TEST_CASE(M) void dummy_test_case_demo##__COUNTER__()

/// a simple macro constant
#define MACRO_CONSTANT 42

/// a more complex macro function
#define MORE_COMPLEX_MACRO(x,...) do {if(x) {__VA_ARGS__}} while(false)

namespace example {

	/**
	 * \include_example{square test}
	 */
	template<auto X>
	constexpr auto square = X*X;

	class My_sub_class final : public My_type {
	  protected:
		int foo()override;
	};
	
	int My_sub_class::foo() {
		return 23;
	}


	void test(int const*);
	void test(int * const);
	void test(const int***);
	[[deprecated("reason")]]
	void test(const int*const**);
	
	TEST_CASE("square test") {
		static_assert(square<0> == 0);
		static_assert(square<1> == 1);
		static_assert(square<2> == 4);
		static_assert(square<3> == 9);
	}
	
}
