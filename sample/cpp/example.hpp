#ifndef INCLUDE_GUARD_THAT_WONT_BE_INCLUDED_IN_THE_DOCUMENTATION
#define INCLUDE_GUARD_THAT_WONT_BE_INCLUDED_IN_THE_DOCUMENTATION

#include <string>
#include <cstddef>
#include <concepts>
#include <iostream>

/// brief description
///
/// another paragraph
/// with multiple lines
/// \param xy The documentation for ARG xy
/// \pre Some precondition
extern "C" __attribute__ ((visibility ("default"))) int MY_global_func(const char* xy);

extern "C" {
	
	typedef struct MY_c_style_struct {
		int x; ///< comment
		float y;
		
		union {
		    int a;
		    float b;
	    } u;
	} MY_c_style_struct;
	
	typedef struct MY_opaque_handle MY_opaque_handle;
	
	void MY_global_func_2(MY_c_style_struct*);

}

/// the namespace
namespace example {

	// forward declaration, that will be ignored in the documentation
	class My_type;
	class My_sub_class;
	
	int f1(const std::string& x) noexcept(sizeof(int)>2);

#define IGNORED_MACRO

/// some macro
#define MY_MACRO(x) (x*x)

	/// \skip
	void impl_detail();

	namespace detail {
		int x;
	}

	/**
	 * An example concept
	 * \rparam a An lvalue
	 */
    template<typename T>
	concept Addable = std::regular<T> && requires(T a) {
		{ add(a, a) } -> std::same_as<T&>;
		{ a.count() } -> std::integral;
	};

	template<Addable T>
	void concept_f1(T) {}
 
	template<typename T>
	requires Addable<T>
	void concept_f(T) {}

	template<class T>
	constexpr bool is_meowable = true;
	
    template<typename T>
	concept Meowable = is_meowable<T>;

	template<typename T>
	void concept_f(T) requires is_meowable<T> {}

	template<Meowable T>
	void concept_f(T) requires(sizeof(T)<16) {}


	template<typename T, typename U>
	requires (sizeof(T)<8)
	struct My_template {};
	
	template<typename T>
	struct My_template<T, int> {
		int xxx = 23;
	};
	
	template<>
	struct My_template<int, int> {
		int xxx = 42;
	};


	using my_typedef = char; ///< using


	typedef char my_typedef_2;///< typedef

	void trailing_comment_test(); ///< trailing comment

	/// auto
	auto test_auto() {
		return 42;
	}
	/// auto&
	auto& test_auto_ref(int& i) {
		return i;
	}
	/// decltype(auto)
	decltype(auto) test_auto_decltype(int& i) {
		return 42;
	}
	/// trailing return type
	auto test_trailing_return(int i) -> decltype(i) {
		return 42;
	}

	Meowable auto cat() {
		return 0;
	}

	/// template-alias
	template<typename T>
	using my_typedef_3 = const T;

	enum [[deprecated("Long reason why one should not use MyEnumA")]] MyEnumA {
		foo, bar
	};
	
	enum class MyEnum : int {
		a = 42,
		b, ///< comment about b
		c = 3,
		d,
		e
	};

	using MyEnumAlias = MyEnum;

	// first function, without any doc-comment
	constexpr int f1()noexcept;

	/**
	 * Doc-String about f1 overload-set\n with multiple
	 *
	 * lines
	 *
	 * \overload_specific
	 *
	 * Comment about the float-overload, specifically.
	 *
	 * @param x X_DESCR
	 * @return RET_DESCR
	 * \throws B only from f1(float)
	 * \throws C thrown by all overloads
	 */
	 [[nodiscard, my_custom_attribute(with, some, args)]]
	constexpr int f1(float x)noexcept;
	
	/**
	 * \overload_specific
	 *
	 * Additional text specific to string overload.\n
	 * Which can consist of multiple lines...
	 *
	 * - and also
	 * - other
	 * - lists
	 *
	 * @param x X_DESCR
	 * @return RET_DESCR
	 * \throws std::exception only from `f1(const std::string&)`
	 * \throws C
	 */
	int f1(const std::string& x) noexcept(sizeof(int)>2);
	
	/**
	 * \overload_specific
	 * two parameters
	 *
	 * Multi-Part std-reference: `std::partial_ordering::unordered`
	 *
	 * @param x X_DESCR
	 * @param y Y_DESCR
	 * @return RET_DESCR
	 * \throws C
	 */
	int f1(float x, int y);

	/// Overload with single comment
	void f2(int);
	void f2(float);

	/// Overload with single comment
	/// \overload_specific
	/// And some specific part on the first
	void f3(int);
	void f3(float);

	/**
	 * some `std::printf()`-like template
	 * \tparam Ts template parameter
	 * \param ts value parameter
	 * \return return value
	 * @todo something that still needs doing
	 *
	 *
	 * another normal paragraph\n\n
	 * with a large line break
	 *
	 *     indentation test
	 *
	 * References to external projects: `glfwInit()`, `std::string`.
	 *
	 * Inline non-C++ code ``#~~>x``
	 *
	 * \danger Danger Danger Danger
	 * \danger This line should also be part of the previous command!
	 *
	 * \danger And this is a new one
	 *
	 *
	 * \code{cpp}
	 * #include <iostream>
	 *
	 * int main() {
	 * 	std::cout << "Example\n";
	 * 	print(42.f);
	 * }
	 * \endcode
	 *
	 * \see `My_type`, `My_other_type`, `f3`
	 */
	template<typename... Ts>
	auto print(Ts&&... ts) {
		return 42.f;
	}

	class My_other_type {
	  public:
		/// hidden friend (should be included in My_other_type)
		friend bool operator==(const My_other_type& lhs, const My_other_type& rhs) {
			return true;
		}
		/// hidden friend (should be included in My_other_type)
		friend bool operator==(const My_other_type& lhs, int rhs) {
			return true;
		}
	};

	/// a class
	///
	/// method ref: `x.method()`
	class My_type {
	  public:
		My_type(int);
		
        template<int N>
		inline My_type& method() requires (N>42) {
			return *this;
		}

		/// hidden friend (should be included in My_type)
		friend bool operator==(const My_type& lhs, const My_type& rhs) {
			return lhs.private_member_ == rhs.private_member_;
		}
		
		template<typename... Ts>
		friend auto print(Ts&&... ts);
		
	  protected:
		virtual int foo() = 0;
		
	  public:
	  friend class Foo;
	
	  private:
		int private_member_ = 3;
	};
	
	template<typename T>
	class Class_template {
	  public:
		Class_template() = default;
		~Class_template() noexcept(false) {}
		
		template<typename U>
		operator U() requires(sizeof(U)==sizeof(T));
		
		void requires_test() requires is_meowable<T>;
	};
	
	namespace detail {
		/// \def_example{example::My_type}
		inline auto example_for_my_type(example::My_type& x) {
			example::My_type* rt = &x.method<43>();
			rt->method<43>().method<43>()
			        .method<43>();
			auto y = example::MyEnum::a;
			return rt;
		}
	}
	
	class Moveonly {
	  public:
		Moveonly(const Moveonly&) = delete;
		Moveonly(Moveonly&&) = default;
		
		Moveonly& operator=(const Moveonly&) = delete;
		Moveonly& operator=(Moveonly&&) = default;
	};
	class Moveonly_sub : public Moveonly {
	};
	
	/// struct example
	struct S {
		int x = 42;
		
		auto operator<=>(const S&) const = default;
	};

	// impl, that will be ignored in documentation
	int f1(const std::string&) noexcept(sizeof(int)>2) {
		return 0;
	}
	
}

namespace std {
	template<>
	struct hash<example::S> {
		std::size_t operator()(const example::S& v)const {
			return static_cast<std::size_t>(v.x);
		}
	};
	
	template<typename T, typename U>
	struct hash<example::My_template<T,U>> {
		std::size_t operator()(const example::My_template<T,U>& v)const {
			return 0;
		}
	};
}

#endif
