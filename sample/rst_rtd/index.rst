UCPP-Doc Example
======================================

.. cpp:ucpp_var_ctx:: example::My_type rt

.. cpp:ucpp_var_ctx:: example::My_type rt->method<43>()


Any included entity can be referenced, like this `example::print`

Example code-blocks
--------------------------------------

Extracted from second_example.hpp

.. ucpp:include_example:: example::My_type


Complete files:
--------------------------------------

.. ucpp:file:: example.hpp



Functions (with all overloads)
--------------------------------------

.. ucpp:function:: example::test


Types
--------------------------------------

.. ucpp:type:: ::example::My_sub_class


Variables
--------------------------------------

.. ucpp:variable:: square


Macros
--------------------------------------

.. ucpp:macro:: MACRO_CONSTANT

.. ucpp:macro:: MORE_COMPLEX_MACRO
