[![License: MIT](https://img.shields.io/badge/License-MIT-green.svg)](/LICENSE)

# Unimaginative C＋＋ Documentation Generator

An embarrassingly simple quick-and-dirty preprocessor that uses libtooling/clang to parse a C++ code base and generate API documentation markup to be consumed by sphinx to generate a HTML site [like this](https://lowkey42.gitlab.io/ucpp_doc/).

Or in other words a...

![](image.jpg)


## Features

- C++20 support on par with the current Clang release (including concepts)
- Compact rendering of overload sets
  - Overloads are listed in declaration order and numbered
  - The first declaration may contain a common prefix, that applies to all overloads (separated by `\overload_specific`, see `f1` functions in sample/cpp/example.hpp)
  - Parts of the documentation that only apply to some of the overloads, are marked with the overloads numbers
- Reference links for documented entities in `.. code-block::` directives
- Extraction of marked function bodies as example code
  - Functions marked with `/// \def_example{Example Name}` or doctest/catch2 Unit-Test (`TEST_CASE("Example Name") {...}`)
  - Can be included as code blocks with either `.. ucpp:include_example:: Example Name` or `\include_example{Example Name}` in Doxygen-Style comments
- Deduction of `auto` return types (for trivial cases)
- Extraction of requirements
- Human-readable formatting of concept definitions
- Generation of web links to STL types/functions and other external libraries
- Automatic detection of some higher level attributes for types
  - `Typedef-Struct`: C-Style `typedef struct {...} Name;` structs
  - `Opaque-Handle`: `typedef struct T T;`
  - `POD` or for Non-POP types one or multiple of: `Aggregate`, `Trivial`, `Standard-Layout`
  - The type category (one of): `Indestructible`, `Immovable`, `Movable`, `Copyable`, `Semi-Regular`, `Regular`
  - `Ordered (strong/weak/partial/unknown)` if `operator<=>` is defined for the type
  - `Abstract`: Pure virtual types
  - `Polymorphic`: Types with at least one virtual function
  - `Hashable`: Types for which `std::hash<T>` is defined
  - `Tuple-Like`: Types for which `std::tuple_size<T>` is defined
- Supports both reStructuredText and a subset of Doxygen commands in comments (as well as an unhealthy mix of both)
  - Supported Doxygen commands:
    - `\tparam Name ...`
    - `\param Name ...`
    - `\return ...`
    - `\throw Name ...`
    - `\pre ...` (for preconditions)
    - `\post`
    - `\invariant`
    - `\author ...`
    - `\todo ...`
    - `\see ...`
    - `\warning ...`
    - `\attention ...`
    - `\note ...`
    - `\verbatim ... \endverbatim`
    - `\code{Language} ... \endcode`
    - `\n` or `\par` to start a new paragraph 
  - Additional Doxygen-Style commands: 
    - `\include_example{...}` (to include a named example)
    - `\rparam Name ...` (for requires clause parameters in concepts)
    - `\overload_specific` (to separate the common and overload specific function description)
    - `\tip ...`
    - `\danger ...`
  - Note that, contrary to Doxygen, references to types/functions/... are not automatically inserted everywhere. Instead, inline code blocks (like `\`this\``) can be used, in which all references will be resolved and linked.
    - Use two quotes (like `\`\`this\`\``) for code blocks that shouldn't be parsed, e.g. because they don't contain C++
- Custom CSS for the Furo Sphinx-Theme
  - Unifies the color scheme of example code blocks and documentation entries
  - Custom styling for attributes and requirements
  - More compact rendering of templates and admonitions
- API documentation is automatically added to the table of contents on the right
  - Can be disabled using `.. rst-class:: hide-toc` in front of `.. ucpp` directives
  - JS-based workaround for the missing support in [Sphinx itself](https://github.com/sphinx-doc/sphinx/issues/6316)


## Requirements
C++ dependencies (build only):
- LibTooling and LLVM >= 15
- Boost iostreams >= 1.70
- CMake >= 3.15

Python dependencies (execution only):
- Sphinx >= 5
- regex
- furo (only to build the sample)

```bash
pip install sphinx furo regex
```


## Limitations

This project is a simple solution to an extremely specific use-case (generating HTML documentations for my C++20 projects) and as such will probably be of limited use for others.

- Limited support for inclusion of single entities (types, functions, ...)
    - No distinction between templates and non-templates when referencing entities to be included
    - `.. ucpp:function::` always includes the entire overload set
- Limited configuration options
- No support for more advanced Doxygen commands, like conditions or groups
- Always includes all entities, except
    - those marked with `/// \skip`
    - declarations from `details`/`detail` namespaces
    - anything from `std`
    - private members
- Limited validation
- Kind of "hacky" architecture (because I wanted to avoid touching Python code as much as possible)
- Untested:
    - Windows/MSVC
    - non-ASCII characters in paths or source files


## Usage

Resulting HTML documentation using the [Furo (recommended)](https://lowkey42.gitlab.io/ucpp_doc/) and [read-the-docs](https://lowkey42.gitlab.io/ucpp_doc/rtd) theme.

index.rst:
```rst
UCPP-Doc Example
======================================

Any included entity can be referenced, like this `example::print`

Example code-blocks
--------------------------------------

Extracted from second_example.hpp

.. ucpp:include_example:: example::My_type


Complete files:
--------------------------------------

.. ucpp:file:: example.hpp



Functions (with all overloads)
--------------------------------------

.. ucpp:function:: example::test


Types
--------------------------------------

.. ucpp:type:: ::example::My_sub_class


Variables
--------------------------------------

.. ucpp:variable:: square


Macros
--------------------------------------

.. ucpp:macro:: MACRO_CONSTANT

.. ucpp:macro:: MORE_COMPLEX_MACRO

```

CMake:
```cmake
FetchContent_Declare(
    ucpp_doc
    GIT_REPOSITORY https://gitlab.com/lowkey42/ucpp_doc.git
    GIT_TAG        master
)
FetchContent_MakeAvailable(ucpp_doc)

ucpp_generate_doc(
	TARGET           doc                             # Name of the generated target
	HEADERS          ${CMAKE_CURRENT_SOURCE_DIR}/cpp # Directories or C++ file to parse 
	SPHINX_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/rst # Root directory of sphinx documentation
	OUTPUT_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/doc # Target directory for the generated HTML
	UCPP_DOC_ARGS    ...                             # Additional parameters passed to ucpp_doc 
	                                                 #   (e.g. "-std=c++17" or "-I/additional/includes")
	SPHINX_ARGS      ...                             # Additional parameters passed to sphinx
	INSTALL                                          # Also adds install(DIRECTORY ${OUTPUT_DIRECTORY} ...)
)
```

conf.py for Sphinx, containing at least:
```python
import os
import sys
sys.path.insert(0, os.path.abspath('.'))

extensions = ['ucpp']
html_static_path = ['_static']

# called to lookup the url for a given source file
def ucpp_resolve(file, line):
    path = os.path.relpath(file, os.path.abspath('../cpp'))
    return "https://gitlab.com/lowkey42/ucpp_doc/-/blob/master/sample/cpp/"+path+"#L"+line

# used to link identifiers to external projects based on a prefix (either a namespace or a C-Style prefix)
# if the option is not specified, it only includes the definition for std::
ucpp_ext_mapping = {
    'std::' : 'https://duckduckgo.com/?q=\\{target} site:en.cppreference.com',
    'glfw' : 'https://duckduckgo.com/?q=\\{target} site:glfw.org/docs'
}
```

